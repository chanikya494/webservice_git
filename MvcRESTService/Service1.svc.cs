﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace MvcRESTService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        string query;
        DatabaseAccess dataclass = new DatabaseAccess();
        DataSet ds;
        public List<sp_zone_result> GetZoneData(string user)
        {
            List<sp_zone_result> strzone = new List<sp_zone_result>();
            try
            {
                query = "SP_DB_View_Zone'" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_zone_result spzone = new sp_zone_result();
                            spzone.zone = row["ZONE"].ToString();
                            strzone.Add(spzone);
                        }
                    }
                }

            }
            catch (Exception)
            {

            }
            return strzone;
        }
        public List<string> GetSampleData(string user)
        {
            List<string> strsample = new List<string>();
            try
            {
                query = "SP_DB_View_Zone '" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            strsample.Add(row["ZONE"].ToString());
                        }
                    }
                }

            }
            catch (Exception)
            {

            }
            return strsample;
        }

        public List<userMaster> getUserName(string userid, string password)
        {
            List<userMaster> userList = new List<userMaster>();
            query = string.Format("select UserName from UserMaster where  userid='{0}' and password='{1}' ", userid, password);
            ds = new DataSet();
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        userMaster user = new userMaster();
                        user.username = ds.Tables[0].Rows[0][0].ToString();
                        user.userid = userid;
                        user.status = "true";
                        userList.Add(user);
                    }

                    else
                    {
                        userMaster user = new userMaster();
                        user.username = "";
                        user.userid = "";
                        user.status = "false";
                        userList.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                userMaster user = new userMaster();
                user.username = "";
                user.userid = "";
                user.status = "false";
                userList.Add(user);
            }

            return userList.ToList();
        }

        public List<sp_circle_result> GetCircleData(string zone, string user)
        {
            List<sp_circle_result> strcircle = new List<sp_circle_result>();
            try
            {
                query = "SP_DB_View_Circle '" + zone + "','" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_circle_result spcircle = new sp_circle_result();
                            spcircle.circle = row["CIRCLE"].ToString();
                            strcircle.Add(spcircle);
                        }
                    }
                }

            }
            catch (Exception)
            {

            }
            return strcircle;
        }

        public List<sp_mla_result> GetMlaData(string zone, string circle, string user)
        {
            List<sp_mla_result> strmla = new List<sp_mla_result>();
            try
            {
                query = "SP_DB_View_MLA '" + zone + "','" + circle + "','" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_mla_result spmla = new sp_mla_result();
                            spmla.mla = row["MLA_CONST"].ToString();
                            strmla.Add(spmla);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return strmla;
        }

        public List<sp_division_result> GetDivisionData(string zone, string circle, string mla, string user)
        {
            List<sp_division_result> strdiv = new List<sp_division_result>();
            try
            {
                query = "SP_DB_View_Division '" + zone + "','" + circle + "','" + mla + "','" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_division_result spdiv = new sp_division_result();
                            spdiv.div = row["div"].ToString();
                            strdiv.Add(spdiv);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return strdiv;
        }

        public List<sp_subdiv_result> GetSubDivisionData(string zone, string circle, string mla, string division, string user)
        {
            List<sp_subdiv_result> strsubdiv = new List<sp_subdiv_result>();
            try
            {
                query = "SP_DB_View_SubDivision '" + zone + "','" + circle + "','" + mla + "','" + division + "','" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_subdiv_result spsubdiv = new sp_subdiv_result();
                            spsubdiv.subdiv = row["subdiv"].ToString();
                            strsubdiv.Add(spsubdiv);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return strsubdiv;
        }

        public List<sp_jen_result> GetJuniorEngineerData(string zone, string circle, string mla, string division, string subdivision, string user)
        {
            List<sp_jen_result> strjen = new List<sp_jen_result>();
            try
            {
                query = "SP_DB_View_Section '" + zone + "','" + circle + "','" + mla + "','" + division + "','" + subdivision + "','" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_jen_result spjen = new sp_jen_result();
                            spjen.jen = row["SECTION"].ToString();
                            strjen.Add(spjen);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return strjen;
        }

        public List<sp_piedata> GetPieData(string user, string zone, string circle, string mla, string division, string subdivision, string jen)
        {
            List<sp_piedata> pielist = new List<sp_piedata>();
            ds = new DataSet();
            query = "User_DB_View_FeederPieChart  '" + user + "','" + zone + "','" + circle + "','" + mla + "','" + division + "','" + subdivision + "','" + jen + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        sp_piedata sppie = new sp_piedata();
                        sppie.singlephase = ds.Tables[0].Rows[0]["SinglePhase"].ToString();
                        sppie.twophase = ds.Tables[0].Rows[0]["TwoPhase"].ToString();
                        sppie.threephase = ds.Tables[0].Rows[0]["ThreePhase"].ToString();
                        sppie.nophase = ds.Tables[0].Rows[0]["NoPhase"].ToString();
                        sppie.notcomm = ds.Tables[0].Rows[0]["NotComm"].ToString();
                        sppie.total = ds.Tables[0].Rows[0]["Total"].ToString();
                        pielist.Add(sppie);
                    }
                }
            }
            catch (Exception)
            {

            }
            return pielist;

        }
        public List<sp_substations> GetSubstations(string user, string zone, string circle, string mla, string division, string subdivision, string jen)
        {
            List<sp_substations> sp_sub = new List<sp_substations>();
            ds = new DataSet();
            query = "DB_View_SubStations '" + user + "','" + zone + "','" + circle + "','" + mla + "','" + division + "','" + subdivision + "','" + jen + "'";
            ds = dataclass.GetData(query);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            DataTable dt1 = new DataTable();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sp_substations clssubstations = new sp_substations();
                clssubstations.feeders = new List<sp_feeders>();
                clssubstations.substation = dt.Rows[i]["SUBSTATION"].ToString();
                clssubstations.colour = dataclass.GetColour(i);
                dt1 = dataclass.GetFeeder("SP_View_Feeders '" + dt.Rows[i]["SUBSTATION"].ToString() + "'");

                if (clssubstations.feeders == null)
                {
                    clssubstations = new sp_substations();
                    clssubstations.feeders = new List<sp_feeders>();
                }

                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    sp_feeders clsfeeder = new sp_feeders();
                    clsfeeder.feeder = dt1.Rows[j]["FeederName"].ToString();
                    clssubstations.feeders.Add(clsfeeder);
                }

                sp_sub.Add(clssubstations);
            }
            return sp_sub.ToList();
        }

        public List<sp_phases> GetPhaseData(string user, string zone, string circle, string mla, string division, string subdivision, string jen, string estatus, string pdate)
        {
            List<sp_phases> phaselist = new List<sp_phases>();
            ds = new DataSet();
            query = "User_DB_View_FeederPhaseSummary  '" + user + "','" + zone + "','" + circle + "','" + mla + "','" + division + "','" + subdivision + "','" + jen + "','" + estatus + "','" + pdate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_phases phase = new sp_phases();
                            phase.SNo = row["SNo"].ToString();
                            phase.Substation = row["SubStation"].ToString();
                            phase.FeederName = row["FeederName"].ToString();
                            phase.Type = row["Type"].ToString();
                            phase.Area = row["Area"].ToString();
                            phase.MeterNo = row["MeterNo"].ToString();
                            phase.MF = row["MF"].ToString();
                            phase.RTC = row["RTC"].ToString();
                            phase.Duration = row["Duration"].ToString();
                            phase.Spells = row["Spells"].ToString();
                            phaselist.Add(phase);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return phaselist;
        }

        public List<sp_HalfHourlyPeakload> getHalfHourlyPeaklod(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string startdate)
        {
            List<sp_HalfHourlyPeakload> halfList = new List<sp_HalfHourlyPeakload>();
            ds = new DataSet();
            query = string.Format("User_DB_View_HalfHourlyPeakLoad @UserId='{7}',@Zone='{0}',@Circle='{1}',@MLA='{2}',@Division='{3}',@SubDivision='{4}',@JEN='{5}',@StartDate='{6}'", zone, circle, mla, division, subdivision, jen, startdate,userid);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_HalfHourlyPeakload load = new sp_HalfHourlyPeakload();
                            load.SNo = ds.Tables[0].Rows[i][0].ToString();
                            load.SUbStation = ds.Tables[0].Rows[i][1].ToString();
                            load.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            load.Type = ds.Tables[0].Rows[i][3].ToString();
                            load.Area = ds.Tables[0].Rows[i][4].ToString();
                            load.MeterNo = ds.Tables[0].Rows[i][5].ToString();
                            load.MF = ds.Tables[0].Rows[i][6].ToString();
                            load.RTC = ds.Tables[0].Rows[i][7].ToString();
                            load.Ir = ds.Tables[0].Rows[i][8].ToString();
                            load.Iy = ds.Tables[0].Rows[i][9].ToString();
                            load.Ib = ds.Tables[0].Rows[i][10].ToString();
                            load.AvgCur = ds.Tables[0].Rows[i][11].ToString();
                            load.kVA = ds.Tables[0].Rows[i][12].ToString();
                            halfList.Add(load);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return halfList.ToList();
        }

        public List<sp_daywisesubstationpeakload> getDaywiseSubstationPeakLoad(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string startdate)
        {
            List<sp_daywisesubstationpeakload> daywiseList = new List<sp_daywisesubstationpeakload>();
            ds = new DataSet();
            query = string.Format("User_DB_View_DaywiseSubStationPeakLoad @UserId='{0}',@Zone='{1}',@Circle='{2}',@MLA='{3}',@Division='{4}',@SubDivision='{5}',@JEN='{6}',@StartDate='{7}'", userid, zone, circle, mla, division, subdivision, jen, startdate);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_daywisesubstationpeakload load = new sp_daywisesubstationpeakload();
                            load.SNo = ds.Tables[0].Rows[i][0].ToString();
                            load.substation = ds.Tables[0].Rows[i][1].ToString();
                            load.meterdate = ds.Tables[0].Rows[i][2].ToString();
                            load.Ir = ds.Tables[0].Rows[i][3].ToString();
                            load.Iy = ds.Tables[0].Rows[i][4].ToString();
                            load.Ib = ds.Tables[0].Rows[i][5].ToString();
                            load.kVa = ds.Tables[0].Rows[i][6].ToString();
                            load.AvgCur = ds.Tables[0].Rows[i][7].ToString();
                            daywiseList.Add(load);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return daywiseList.ToList();
        }

        public List<sp_PfAbstract> getPfAbstract(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string pfdate)
        {
            List<sp_PfAbstract> pflist = new List<sp_PfAbstract>();
            ds = new DataSet();
            query = string.Format("User_DB_View_PFAbstract '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'", userid, zone, circle, mla, division, subdivision, jen, pfdate);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_PfAbstract abstarct = new sp_PfAbstract();
                            abstarct.SNo = ds.Tables[0].Rows[i][0].ToString();
                            abstarct.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            abstarct.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            abstarct.Type = ds.Tables[0].Rows[i][3].ToString();
                            abstarct.Area = ds.Tables[0].Rows[i][4].ToString();
                            abstarct.Meterno = ds.Tables[0].Rows[i][5].ToString();
                            abstarct.AbovePF = ds.Tables[0].Rows[i][6].ToString();
                            abstarct.BelowPf = ds.Tables[0].Rows[i][7].ToString();
                            pflist.Add(abstarct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return pflist.ToList();
        }

        public List<sp_overUnderload> getOverload(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string oudate)
        {
            List<sp_overUnderload> overloadList = new List<sp_overUnderload>();
            ds = new DataSet();
            query = string.Format("User_DB_View_OverLoad_UnderLoad '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'", userid, zone, circle, mla, division, subdivision, jen, oudate);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_overUnderload load = new sp_overUnderload();
                            load.SNo = ds.Tables[0].Rows[i][0].ToString();
                            load.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            load.MeterDate = ds.Tables[0].Rows[i][2].ToString();
                            load.Overload = ds.Tables[0].Rows[i][3].ToString();
                            load.Underload = ds.Tables[0].Rows[i][4].ToString();
                            overloadList.Add(load);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return overloadList.ToList();
        }

        public List<sp_feederInterruption> getFeederInterruption(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string itrdate)
        {
            List<sp_feederInterruption> feederList = new List<sp_feederInterruption>();
            ds = new DataSet();
            query = string.Format("User_DB_View_FeederInterruptionAbstract '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'", userid, zone, circle, mla, division, subdivision, jen, itrdate);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_feederInterruption feeder = new sp_feederInterruption();
                            feeder.SNo = ds.Tables[0].Rows[i][0].ToString();
                            feeder.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            feeder.MeterDate = ds.Tables[0].Rows[i][2].ToString();
                            feeder.PF1 = ds.Tables[0].Rows[i][3].ToString();
                            feeder.PF2 = ds.Tables[0].Rows[i][4].ToString();
                            feeder.PF3 = ds.Tables[0].Rows[i][5].ToString();
                            feeder.PF4 = ds.Tables[0].Rows[i][6].ToString();
                            feederList.Add(feeder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederList.ToList();
        }

        public List<sp_feederInstants> getFeederInstants(string userid, string zone, string circle, string mla, string division, string subdivision, string jen)
        {
            List<sp_feederInstants> instantsList = new List<sp_feederInstants>();
            ds = new DataSet();
            query = string.Format("User_DB_View_FeederEnergyParameters '{0}','{1}','{2}','{3}','{4}','{5}','{6}'", userid, zone, circle, mla, division, subdivision, jen);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_feederInstants instants = new sp_feederInstants();
                            instants.SNo = ds.Tables[0].Rows[i][0].ToString();
                            instants.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            instants.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            instants.Type = ds.Tables[0].Rows[i][3].ToString();
                            instants.Area = ds.Tables[0].Rows[i][4].ToString();
                            instants.MeterNo = ds.Tables[0].Rows[i][5].ToString();
                            instants.MF = ds.Tables[0].Rows[i][6].ToString();
                            instants.RTC = ds.Tables[0].Rows[i][7].ToString();
                            instants.ActEng_CumFWD = ds.Tables[0].Rows[i][8].ToString();
                            instants.ReaEng_CumFWD = ds.Tables[0].Rows[i][9].ToString();
                            instants.ReaEng_CumREV = ds.Tables[0].Rows[i][10].ToString();
                            instants.AppEng_CumFWD = ds.Tables[0].Rows[i][11].ToString();
                            instants.avgPF_FWD = ds.Tables[0].Rows[i][12].ToString();
                            instants.Freq = ds.Tables[0].Rows[i][13].ToString();
                            instantsList.Add(instants);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return instantsList.ToList();
        }

        public List<sp_feederNotCommunicating> getFeederNotComm(string userid, string zone, string circle, string mla, string division, string subdivision, string jen)
        {
            List<sp_feederNotCommunicating> notCommList = new List<sp_feederNotCommunicating>();
            ds = new DataSet();
            query = string.Format("User_DB_View_NotCommunicated '{0}','{1}','{2}','{3}','{4}','{5}','{6}'", userid, zone, circle, mla, division, subdivision, jen);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_feederNotCommunicating notComm = new sp_feederNotCommunicating();
                            notComm.SNo = ds.Tables[0].Rows[i][0].ToString();
                            notComm.Zone = ds.Tables[0].Rows[i][1].ToString();
                            notComm.Circle = ds.Tables[0].Rows[i][2].ToString();
                            notComm.MLA = ds.Tables[0].Rows[i][3].ToString();
                            notComm.Division = ds.Tables[0].Rows[i][4].ToString();
                            notComm.SubDivision = ds.Tables[0].Rows[i][5].ToString();
                            notComm.Section = ds.Tables[0].Rows[i][6].ToString();
                            notComm.SubStation = ds.Tables[0].Rows[i][7].ToString();
                            notComm.FeederName = ds.Tables[0].Rows[i][8].ToString();
                            notComm.Type = ds.Tables[0].Rows[i][9].ToString();
                            notComm.Area = ds.Tables[0].Rows[i][10].ToString();
                            notComm.Meterno = ds.Tables[0].Rows[i][11].ToString();
                            notComm.MF = ds.Tables[0].Rows[i][12].ToString();
                            notComm.RegDate = ds.Tables[0].Rows[i][13].ToString();
                            notComm.Schdule = ds.Tables[0].Rows[i][14].ToString();
                            notComm.CommDate = ds.Tables[0].Rows[i][15].ToString();
                            notComm.EventStatus = ds.Tables[0].Rows[i][16].ToString();
                            notComm.Remarks = ds.Tables[0].Rows[i][17].ToString();
                            notCommList.Add(notComm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return notCommList.ToList();
        }

        public List<sp_LogParent> getLogParent(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string logdate)
        {
            List<sp_LogParent> parentList = new List<sp_LogParent>();
            ds = new DataSet();
            query = string.Format("User_DB_View_SubStationLogBook_Events @UserId='{0}',@Zone='{1}',@Circle='{2}',@MLA='{3}',@Division='{4}',@SubDivision='{5}',@JEN='{6}',@StartDate='{7}',@EndDate='{8}',@IntialParam='{9}'", userid, zone, circle, mla, division, subdivision, jen, logdate, logdate, "Initial");
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_LogParent parent = new sp_LogParent();
                            parent.Substation = ds.Tables[0].Rows[i][0].ToString();
                            parent.FeederName = ds.Tables[0].Rows[i][1].ToString();
                            parent.Type = ds.Tables[0].Rows[i][2].ToString();
                            parent.Area = ds.Tables[0].Rows[i][3].ToString();
                            parent.Meterno = ds.Tables[0].Rows[i][4].ToString();
                            parentList.Add(parent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return parentList.ToList();
        }

        public List<sp_LogChild> getLogChild(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string logdate, string meterno)
        {
            List<sp_LogChild> childList = new List<sp_LogChild>();
            ds = new DataSet();
            query = string.Format("User_DB_View_SubStationLogBook_Events @UserId='{0}',@Zone='{1}',@Circle='{2}',@MLA='{3}',@Division='{4}',@SubDivision='{5}',@JEN='{6}',@StartDate='{7}',@EndDate='{8}',@MeterNo='{9}'", userid, zone, circle, mla, division, subdivision, jen, logdate, logdate, meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_LogChild child = new sp_LogChild();
                            child.SNo = ds.Tables[0].Rows[i][0].ToString();
                            child.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            child.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            child.Type = ds.Tables[0].Rows[i][3].ToString();
                            child.Area = ds.Tables[0].Rows[i][4].ToString();
                            child.Meterno = ds.Tables[0].Rows[i][5].ToString();
                            child.MF = ds.Tables[0].Rows[i][6].ToString();
                            child.DateTime = ds.Tables[0].Rows[i][7].ToString();
                            child.load_cur = ds.Tables[0].Rows[i][8].ToString();
                            child.load_kva = ds.Tables[0].Rows[i][9].ToString();
                            child.Reading = ds.Tables[0].Rows[i][10].ToString();
                            child.kwh_Cons = ds.Tables[0].Rows[i][11].ToString();
                            child.Ir = ds.Tables[0].Rows[i][12].ToString();
                            child.Iy = ds.Tables[0].Rows[i][13].ToString();
                            child.Ib = ds.Tables[0].Rows[i][14].ToString();
                            child.Vr = ds.Tables[0].Rows[i][15].ToString();
                            child.Vy = ds.Tables[0].Rows[i][16].ToString();
                            child.Vb = ds.Tables[0].Rows[i][17].ToString();
                            child.kvarh_lag = ds.Tables[0].Rows[i][18].ToString();
                            child.kvarh_lead = ds.Tables[0].Rows[i][19].ToString();
                            child.PF = ds.Tables[0].Rows[0][20].ToString();
                            child.EventDesc = ds.Tables[0].Rows[i][21].ToString();
                            childList.Add(child);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return childList.ToList();

        }

        public List<feederDetails> getFeeder(string meterno)
        {
            List<feederDetails> feederList = new List<feederDetails>();
            ds = new DataSet();
            query = string.Format("DB_View_FeederDetails '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            feederDetails feeder = new feederDetails();
                            feeder.meterno = ds.Tables[0].Rows[0]["MeterNo"].ToString();
                            feeder.feederNo = ds.Tables[0].Rows[0]["FeederNo"].ToString();
                            feeder.feederName = ds.Tables[0].Rows[0]["FeederName"].ToString();
                            feeder.zone = ds.Tables[0].Rows[0]["Zone"].ToString();
                            feeder.circle = ds.Tables[0].Rows[0]["Circle"].ToString();
                            feeder.mla = ds.Tables[0].Rows[0]["MLA"].ToString();
                            feeder.division = ds.Tables[0].Rows[0]["Division"].ToString();
                            feeder.subdivision = ds.Tables[0].Rows[0]["SubDivision"].ToString();
                            feeder.jen = ds.Tables[0].Rows[0]["JEN"].ToString();
                            feeder.substation = ds.Tables[0].Rows[0]["Substation"].ToString();
                            feeder.substation_ph = ds.Tables[0].Rows[0]["SubstationPh"].ToString();
                            feeder.feederType = ds.Tables[0].Rows[0]["FeederType"].ToString();
                            feeder.village = ds.Tables[0].Rows[0]["Village"].ToString();
                            feeder.latitude = ds.Tables[0].Rows[0]["Latitude"].ToString();
                            feeder.longitude = ds.Tables[0].Rows[0]["Longitude"].ToString();
                            feeder.ce_name = ds.Tables[0].Rows[0]["CE_Name"].ToString();
                            feeder.ce_ph = ds.Tables[0].Rows[0]["CE_Ph"].ToString();
                            feeder.se_name = ds.Tables[0].Rows[0]["SE_Name"].ToString();
                            feeder.se_ph = ds.Tables[0].Rows[0]["SE_Ph"].ToString();
                            feeder.xen_name = ds.Tables[0].Rows[0]["XEN_Name"].ToString();
                            feeder.xen_ph = ds.Tables[0].Rows[0]["XEN_Ph"].ToString();
                            feeder.aen_name = ds.Tables[0].Rows[0]["AEN_Name"].ToString();
                            feeder.aen_ph = ds.Tables[0].Rows[0]["AEN_Ph"].ToString();
                            feeder.jen_name = ds.Tables[0].Rows[0]["JEN_Name"].ToString();
                            feeder.jen_ph = ds.Tables[0].Rows[0]["JEN_Ph"].ToString();
                            feeder.fi_name = ds.Tables[0].Rows[0]["FI_Name"].ToString();
                            feeder.fi_ph = ds.Tables[0].Rows[0]["FI_Ph"].ToString();
                            feeder.mla_name = ds.Tables[0].Rows[0]["MLA_Name"].ToString();
                            feeder.mla_ph = ds.Tables[0].Rows[0]["MLA_Ph"].ToString();
                            feeder.modem_sno = ds.Tables[0].Rows[0]["ModemSno"].ToString();
                            feeder.modem_ph = ds.Tables[0].Rows[0]["Modem_PH"].ToString();
                            feeder.metertype = ds.Tables[0].Rows[0]["MeterType"].ToString();
                            feeder.meterFirm = ds.Tables[0].Rows[0]["MeterFirmware"].ToString();
                            feeder.YOM = ds.Tables[0].Rows[0]["YOM"].ToString();
                            feeder.ctr_inter = ds.Tables[0].Rows[0]["InternalCTR"].ToString();
                            feeder.ptr_inter = ds.Tables[0].Rows[0]["InternalPTR"].ToString();
                            feeder.MF = ds.Tables[0].Rows[0]["MF"].ToString();
                            feeder.area = ds.Tables[0].Rows[0]["Area"].ToString();
                            feederList.Add(feeder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederList.ToList();
        }

        public List<sp_Feeder_loadSurvey> getFeederLoadSurvey(string meterno)
        {
            List<sp_Feeder_loadSurvey> feederList = new List<sp_Feeder_loadSurvey>();
            ds = new DataSet();
            query = "DB_View_Feeder_LoadSurvey '" + meterno + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_Feeder_loadSurvey load = new sp_Feeder_loadSurvey();
                            load.sno = ds.Tables[0].Rows[i][0].ToString();
                            load.meterno = ds.Tables[0].Rows[i][1].ToString();
                            load.meterDate = ds.Tables[0].Rows[i][2].ToString();
                            load.vr = ds.Tables[0].Rows[i][3].ToString();
                            load.vy = ds.Tables[0].Rows[i][4].ToString();
                            load.vb = ds.Tables[0].Rows[i][5].ToString();
                            load.ir = ds.Tables[0].Rows[i][6].ToString();
                            load.iy = ds.Tables[0].Rows[i][7].ToString();
                            load.ib = ds.Tables[0].Rows[i][8].ToString();
                            load.kvarh_lag = ds.Tables[0].Rows[i][9].ToString();
                            load.kvarh_lead = ds.Tables[0].Rows[i][10].ToString();
                            load.kvah = ds.Tables[0].Rows[i][11].ToString();
                            load.frequ = ds.Tables[0].Rows[i][12].ToString();
                            feederList.Add(load);
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederList;
        }

        public List<sp_phasehistories> GetPhaseHistoryData(string eventstatus, string meterno, string fromdate, string todate)
        {
            List<sp_phasehistories> phasehistorylist = new List<sp_phasehistories>();
            ds = new DataSet();
            query = "User_DB_View_FeederPhaseSummary  @EventStatus='" + eventstatus + "',@MeterNo='" + meterno + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_phasehistories phasehistory = new sp_phasehistories();
                            phasehistory.SNo = row["SNo"].ToString();
                            phasehistory.Substation = row["SubStation"].ToString();
                            phasehistory.FeederName = row["FeederName"].ToString();
                            phasehistory.Type = row["Type"].ToString();
                            phasehistory.Area = row["Area"].ToString();
                            phasehistory.MeterNo = row["MeterNo"].ToString();
                            phasehistory.MF = row["MF"].ToString();
                            phasehistory.StartDate = row["startdate"].ToString();
                            phasehistory.EndDate = row["enddate"].ToString();
                            phasehistory.Duration = row["Duration"].ToString();
                            phasehistorylist.Add(phasehistory);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return phasehistorylist;
        }
        public List<sp_HalfHourlyPeakload> GetHalfPeakloadHistoryData(string meterno, string fromdate, string todate)
        {
            List<sp_HalfHourlyPeakload> halfList = new List<sp_HalfHourlyPeakload>();
            ds = new DataSet();
            query = "User_DB_View_HalfHourlyPeakLoad @MeterNo='" + meterno + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_HalfHourlyPeakload load = new sp_HalfHourlyPeakload();
                            load.SNo = ds.Tables[0].Rows[i][0].ToString();
                            load.SUbStation = ds.Tables[0].Rows[i][1].ToString();
                            load.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            load.Type = ds.Tables[0].Rows[i][3].ToString();
                            load.Area = ds.Tables[0].Rows[i][4].ToString();
                            load.MeterNo = ds.Tables[0].Rows[i][5].ToString();
                            load.MF = ds.Tables[0].Rows[i][6].ToString();
                            load.RTC = ds.Tables[0].Rows[i][7].ToString();
                            load.Ir = ds.Tables[0].Rows[i][8].ToString();
                            load.Iy = ds.Tables[0].Rows[i][9].ToString();
                            load.Ib = ds.Tables[0].Rows[i][10].ToString();
                            load.AvgCur = ds.Tables[0].Rows[i][11].ToString();
                            load.kVA = ds.Tables[0].Rows[i][12].ToString();
                            halfList.Add(load);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return halfList.ToList();
        }
        public List<sp_areawise> GetAreawiseData(string substation, string feedername)
        {
            List<sp_areawise> areawise = new List<sp_areawise>();
            ds = new DataSet();
            query = "DB_View_GetAreaDetails @SubStation='" + substation.Replace('.', '/') + "',@FeederName='" + feedername + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_areawise area = new sp_areawise();
                            area.zone = row["Zone"].ToString();
                            area.circle = row["Circle"].ToString();
                            area.mla = row["MLA_Const"].ToString();
                            area.division = row["Division"].ToString();
                            area.subdivision = row["SubDivision"].ToString();
                            area.section = row["Section"].ToString();
                            areawise.Add(area);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return areawise;
        }


        public List<sp_daywisesubstationpeakload> GetSubstationPeakloadHistoryData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string substation)
        {
            List<sp_daywisesubstationpeakload> daywiseList = new List<sp_daywisesubstationpeakload>();
            ds = new DataSet();
            query = "User_DB_View_DaywiseSubStationPeakLoad @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "',@SubStation='" + substation + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_daywisesubstationpeakload load = new sp_daywisesubstationpeakload();
                            load.SNo = ds.Tables[0].Rows[i][0].ToString();
                            load.substation = ds.Tables[0].Rows[i][1].ToString();
                            load.meterdate = ds.Tables[0].Rows[i][2].ToString();
                            load.Ir = ds.Tables[0].Rows[i][3].ToString();
                            load.Iy = ds.Tables[0].Rows[i][4].ToString();
                            load.Ib = ds.Tables[0].Rows[i][5].ToString();
                            load.kVa = ds.Tables[0].Rows[i][6].ToString();
                            load.AvgCur = ds.Tables[0].Rows[i][7].ToString();
                            daywiseList.Add(load);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return daywiseList.ToList();
        }

        public List<sp_PfAbstractHistory> GetPFAbstractHistoryData(string meterno, string fromdate, string todate, string pf)
        {
            List<sp_PfAbstractHistory> pfList = new List<sp_PfAbstractHistory>();
            ds = new DataSet();
            query = "User_DB_View_PFAbstract @MeterNo='" + meterno + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "',@PF='" + pf + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_PfAbstractHistory pfHistory = new sp_PfAbstractHistory();
                            pfHistory.SNo = ds.Tables[0].Rows[i][0].ToString();
                            pfHistory.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            pfHistory.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            pfHistory.Type = ds.Tables[0].Rows[i][3].ToString();
                            pfHistory.Area = ds.Tables[0].Rows[i][4].ToString();
                            pfHistory.Meterno = ds.Tables[0].Rows[i][5].ToString();
                            pfHistory.meterdate = ds.Tables[0].Rows[i][6].ToString();
                            pfHistory.PF = ds.Tables[0].Rows[i][7].ToString();
                            pfList.Add(pfHistory);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return pfList.ToList();
        }
        public List<sp_overUnderload> GetOverloadHistoryData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate)
        {
            List<sp_overUnderload> loadList = new List<sp_overUnderload>();
            ds = new DataSet();
            query = "User_DB_View_OverLoad_UnderLoad @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_overUnderload load = new sp_overUnderload();
                            load.SNo = ds.Tables[0].Rows[i][0].ToString();
                            load.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            load.MeterDate = ds.Tables[0].Rows[i][2].ToString();
                            load.Overload = ds.Tables[0].Rows[i][3].ToString();
                            load.Underload = ds.Tables[0].Rows[i][4].ToString();
                            loadList.Add(load);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return loadList.ToList();
        }
        public List<sp_overloadParent> GetOverloadParentData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string substation, string load)
        {
            List<sp_overloadParent> ploadList = new List<sp_overloadParent>();
            ds = new DataSet();
            query = "User_DB_View_OverLoad_UnderLoad @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "',@SubStation='" + substation + "',@Load='" + load + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_overloadParent pload = new sp_overloadParent();
                            pload.SNo = ds.Tables[0].Rows[i][0].ToString();
                            pload.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            pload.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            pload.Type = ds.Tables[0].Rows[i][3].ToString();
                            pload.METERNO = ds.Tables[0].Rows[i][4].ToString();
                            pload.spells = ds.Tables[0].Rows[i][5].ToString();
                            ploadList.Add(pload);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return ploadList.ToList();
        }

        public List<sp_overloadChild> GetOverloadChildData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string meterno, string load)
        {
            List<sp_overloadChild> ploadList = new List<sp_overloadChild>();
            ds = new DataSet();
            query = "User_DB_View_OverLoad_UnderLoad @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "',@MeterNo='" + meterno + "',@Load='" + load + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_overloadChild pload = new sp_overloadChild();
                            pload.SNo = ds.Tables[0].Rows[i][0].ToString();
                            pload.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            pload.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            pload.Type = ds.Tables[0].Rows[i][3].ToString();
                            pload.METERNO = ds.Tables[0].Rows[i][4].ToString();
                            pload.Meterdate = ds.Tables[0].Rows[i][5].ToString();
                            pload.Ir = ds.Tables[0].Rows[i][6].ToString();
                            pload.Iy = ds.Tables[0].Rows[i][7].ToString();
                            pload.Ib = ds.Tables[0].Rows[i][8].ToString();
                            pload.AvgCur = ds.Tables[0].Rows[i][9].ToString();
                            ploadList.Add(pload);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return ploadList.ToList();
        }
        public List<sp_feederInterruption> GetFeederInterruptionHistory(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate)
        {
            List<sp_feederInterruption> feederList = new List<sp_feederInterruption>();
            ds = new DataSet();
            query = "User_DB_View_FeederInterruptionAbstract @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_feederInterruption feeder = new sp_feederInterruption();
                            feeder.SNo = ds.Tables[0].Rows[i][0].ToString();
                            feeder.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            feeder.MeterDate = ds.Tables[0].Rows[i][2].ToString();
                            feeder.PF1 = ds.Tables[0].Rows[i][3].ToString();
                            feeder.PF2 = ds.Tables[0].Rows[i][4].ToString();
                            feeder.PF3 = ds.Tables[0].Rows[i][5].ToString();
                            feeder.PF4 = ds.Tables[0].Rows[i][6].ToString();
                            feederList.Add(feeder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederList.ToList();
        }
        public List<sp_overloadParent> GetFeederInterruptionParentData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string substation, string pfspell)
        {
            List<sp_overloadParent> interParentList = new List<sp_overloadParent>();
            ds = new DataSet();
            query = "User_DB_View_FeederInterruptionAbstract @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "',@SubStation='" + substation + "',@PFSpells='" + pfspell + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_overloadParent feeder = new sp_overloadParent();
                            feeder.SNo = ds.Tables[0].Rows[i][0].ToString();
                            feeder.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            feeder.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            feeder.Type = ds.Tables[0].Rows[i][3].ToString();
                            feeder.METERNO = ds.Tables[0].Rows[i][4].ToString();
                            feeder.spells = ds.Tables[0].Rows[i][5].ToString();
                            interParentList.Add(feeder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return interParentList.ToList();
        }
        public List<sp_interruptionChild> GetFeederInterruptionChildData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string meterno, string pfspell)
        {
            List<sp_interruptionChild> feederChildList = new List<sp_interruptionChild>();
            ds = new DataSet();
            query = "User_DB_View_FeederInterruptionAbstract @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "',@MeterNo='" + meterno + "',@PFSpells='" + pfspell + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_interruptionChild feeder = new sp_interruptionChild();
                            feeder.SNo = ds.Tables[0].Rows[i][0].ToString();
                            feeder.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            feeder.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            feeder.Type = ds.Tables[0].Rows[i][3].ToString();
                            feeder.METERNO = ds.Tables[0].Rows[i][4].ToString();
                            feeder.Startdate = ds.Tables[0].Rows[i][5].ToString();
                            feeder.EndDate = ds.Tables[0].Rows[i][6].ToString();
                            feeder.Duration = ds.Tables[0].Rows[i][7].ToString();
                            feederChildList.Add(feeder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederChildList.ToList();
        }
        public List<sp_feederInstants> GetMeterInstantsHistory(string meterno, string fromdate, string todate)
        {
            List<sp_feederInstants> instantsList = new List<sp_feederInstants>();
            ds = new DataSet();
            query = "User_DB_View_FeederEnergyParameters @MeterNo='" + meterno + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_feederInstants instants = new sp_feederInstants();
                            instants.SNo = ds.Tables[0].Rows[i][0].ToString();
                            instants.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            instants.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            instants.Type = ds.Tables[0].Rows[i][3].ToString();
                            instants.Area = ds.Tables[0].Rows[i][4].ToString();
                            instants.MeterNo = ds.Tables[0].Rows[i][5].ToString();
                            instants.MF = ds.Tables[0].Rows[i][6].ToString();
                            instants.RTC = ds.Tables[0].Rows[i][7].ToString();
                            instants.ActEng_CumFWD = ds.Tables[0].Rows[i][8].ToString();
                            instants.ReaEng_CumFWD = ds.Tables[0].Rows[i][9].ToString();
                            instants.ReaEng_CumREV = ds.Tables[0].Rows[i][10].ToString();
                            instants.AppEng_CumFWD = ds.Tables[0].Rows[i][11].ToString();
                            instants.avgPF_FWD = ds.Tables[0].Rows[i][12].ToString();
                            instants.Freq = ds.Tables[0].Rows[i][13].ToString();
                            instantsList.Add(instants);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return instantsList.ToList();
        }
        public List<sp_LogChild> GetLogBookHistory(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string meterno)
        {
            List<sp_LogChild> logHistoryList = new List<sp_LogChild>();
            ds = new DataSet();
            query = "User_DB_View_SubStationLogBook_Events @UserId='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@MLA='" + mla + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@JEN='" + jen + "',@StartDate='" + fromdate + "',@EndDate='" + todate + "',@MeterNo='" + meterno + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_LogChild logHistory = new sp_LogChild();
                            logHistory.SNo = ds.Tables[0].Rows[i][0].ToString();
                            logHistory.SubStation = ds.Tables[0].Rows[i][1].ToString();
                            logHistory.FeederName = ds.Tables[0].Rows[i][2].ToString();
                            logHistory.Type = ds.Tables[0].Rows[i][3].ToString();
                            logHistory.Area = ds.Tables[0].Rows[i][4].ToString();
                            logHistory.Meterno = ds.Tables[0].Rows[i][5].ToString();
                            logHistory.MF = ds.Tables[0].Rows[i][6].ToString();
                            logHistory.DateTime = ds.Tables[0].Rows[i][7].ToString();
                            logHistory.load_cur = ds.Tables[0].Rows[i][8].ToString();
                            logHistory.load_kva = ds.Tables[0].Rows[i][9].ToString();
                            logHistory.Reading = ds.Tables[0].Rows[i][10].ToString();
                            logHistory.kwh_Cons = ds.Tables[0].Rows[i][11].ToString();
                            logHistory.Ir = ds.Tables[0].Rows[i][12].ToString();
                            logHistory.Iy = ds.Tables[0].Rows[i][13].ToString();
                            logHistory.Ib = ds.Tables[0].Rows[i][14].ToString();
                            logHistory.Vr = ds.Tables[0].Rows[i][15].ToString();
                            logHistory.Vy = ds.Tables[0].Rows[i][16].ToString();
                            logHistory.Vb = ds.Tables[0].Rows[i][17].ToString();
                            logHistory.kvarh_lag = ds.Tables[0].Rows[i][18].ToString();
                            logHistory.kvarh_lead = ds.Tables[0].Rows[i][19].ToString();
                            logHistory.PF = ds.Tables[0].Rows[0][20].ToString();
                            logHistoryList.Add(logHistory);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return logHistoryList.ToList();

        }
        public List<sp_drpsubstations> GetDropdownSubstationData(string user)
        {
            List<sp_drpsubstations> strdrpsub = new List<sp_drpsubstations>();
            try
            {
                query = "SP_DB_View_SubStation '" + user + "'";
                ds = new DataSet();
                ds = dataclass.GetData(query);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            sp_drpsubstations spsub = new sp_drpsubstations();
                            spsub.substation = row["SUBSTATION"].ToString();
                            strdrpsub.Add(spsub);
                        }
                    }
                }

            }
            catch (Exception)
            {

            }
            return strdrpsub;
        }
        public List<sp_livefeedersdata> GetLiveFeedersData(string substation, string pdate)
        {
            List<sp_livefeedersdata> liveFeeederList = new List<sp_livefeedersdata>();
            ds = new DataSet();
            query = "SP_DB_VIEW_FeederStatus '" + substation + "','" + pdate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_livefeedersdata liveFeeder = new sp_livefeedersdata();
                            liveFeeder.EVENTSTATUS = ds.Tables[0].Rows[i][1].ToString();
                            liveFeeder.reserved = Convert.ToInt32(ds.Tables[0].Rows[i][4].ToString());
                            liveFeeder.STARTDATE = Convert.ToDouble(ds.Tables[0].Rows[i][7]);
                            liveFeeder.ENDDATE = Convert.ToDouble(ds.Tables[0].Rows[i][8].ToString());
                            liveFeeder.color = ds.Tables[0].Rows[i][5].ToString();
                            liveFeeederList.Add(liveFeeder);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return liveFeeederList.ToList();
        }
        public List<sp_livefeedername> GetLiveFeederNamesData(string substation)
        {
            List<sp_livefeedername> liveFeeedernames = new List<sp_livefeedername>();
            ds = new DataSet();
            query = "SP_View_Feeders '" + substation + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_livefeedername liveFeedername = new sp_livefeedername();
                            liveFeedername.FeederName = ds.Tables[0].Rows[i][0].ToString();
                            liveFeeedernames.Add(liveFeedername);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return liveFeeedernames.ToList();
        }
        public List<sp_livesummary> GetLiveSummaryData(string substation, string pdate)
        {
            List<sp_livesummary> liveSummaryList = new List<sp_livesummary>();
            ds = new DataSet();
            query = "SP_DB_VIEW_FeederStatus_Summary '" + substation + "','" + pdate + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_livesummary liveSummary = new sp_livesummary();
                            liveSummary.FeederName = ds.Tables[0].Rows[i][0].ToString();
                            liveSummary.FeederNo = ds.Tables[0].Rows[i][1].ToString();
                            liveSummary.MeterNo = ds.Tables[0].Rows[i][2].ToString();
                            liveSummary.FeederType = ds.Tables[0].Rows[i][3].ToString();
                            liveSummary.Dur_3Ph = ds.Tables[0].Rows[i][4].ToString();
                            liveSummary.Dur_2Ph = ds.Tables[0].Rows[i][5].ToString();
                            liveSummary.Dur_1Ph = ds.Tables[0].Rows[i][6].ToString();
                            liveSummary.Dur_PF = ds.Tables[0].Rows[i][7].ToString();
                            liveSummary.Dur_ND = ds.Tables[0].Rows[i][8].ToString();
                            liveSummaryList.Add(liveSummary);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return liveSummaryList.ToList();
        }
        public List<sp_powerfactorgraph> GetPowerFactorGraph(string meterno)
        {
            List<sp_powerfactorgraph> pflist = new List<sp_powerfactorgraph>();
            ds = new DataSet();
            query = "SP_DB_View_Graph '" + meterno + "','Power Factor'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_powerfactorgraph powerfactor = new sp_powerfactorgraph();
                            powerfactor.time = ds.Tables[0].Rows[i][3].ToString();
                            powerfactor.pf = ds.Tables[0].Rows[i][4].ToString();
                            pflist.Add(powerfactor);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return pflist.ToList();

        }
        public List<sp_peakloadgraph> GetHalfPeakloadGraph(string meterno)
        {
            List<sp_peakloadgraph> peakloadlist = new List<sp_peakloadgraph>();
            ds = new DataSet();
            query = "SP_DB_View_Graph '" + meterno + "','KVA'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_peakloadgraph peakload = new sp_peakloadgraph();
                            peakload.time = ds.Tables[0].Rows[i][3].ToString();
                            peakload.kva = ds.Tables[0].Rows[i][4].ToString();
                            peakloadlist.Add(peakload);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return peakloadlist.ToList();

        }
        public List<sp_peakloadgraph> GetSubstationPeakloadGraph(string substation)
        {
            List<sp_peakloadgraph> peakloadlist = new List<sp_peakloadgraph>();
            ds = new DataSet();
            query = "SP_DB_View_SubStationGraph '" + substation + "','KVA'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_peakloadgraph peakload = new sp_peakloadgraph();
                            peakload.time = ds.Tables[0].Rows[i][2].ToString();
                            peakload.kva = ds.Tables[0].Rows[i][3].ToString();
                            peakloadlist.Add(peakload);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return peakloadlist.ToList();

        }
        public List<sp_overloadgraph> GetOverloadGraph(string substation)
        {
            List<sp_overloadgraph> overloadlist = new List<sp_overloadgraph>();
            ds = new DataSet();
            query = "SP_DB_View_SubStationGraph '" + substation + "','Current'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_overloadgraph overload = new sp_overloadgraph();
                            overload.time = ds.Tables[0].Rows[i][2].ToString();
                            overload.AvgCurrent = ds.Tables[0].Rows[i][6].ToString();
                            overloadlist.Add(overload);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return overloadlist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseAllData(string userid, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA @UserID='" + userid + "',@Optionalparam='ALL',@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][2].ToString());
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][3] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][3].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][4] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][4].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseZoneHistoryData(string userid, string zone, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA  @UserID='" + userid + "',@Zone='" + zone + "',@Feeders=1,@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][14].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][15].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseCircleData(string userid, string zone, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA '" + userid + "','" + zone + "',@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][3].ToString());
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][4] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][4].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseCircleHistoryData(string userid, string zone, string circle, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Feeders=1,@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][14].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][15].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseDivisionData(string userid, string zone, string circle, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA '" + userid + "','" + zone + "','" + circle + "',@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][4].ToString());
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][10].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseDivisionHistoryData(string userid, string zone, string circle, string division, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@Feeders=1,@MonthYear = '" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][14].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][15].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseSubDivisionData(string userid, string zone, string circle, string division, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace("$", "&") + "',@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][5].ToString());
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][10].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][11].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseSubDivisionHistoryData(string userid, string zone, string circle, string division, string subdivision, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace("$", "&") + "',@SubDivision='" + subdivision.Replace("$", "&") + "',@Feeders =1,@MonthYear = '" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][14].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][15].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace("$", "&") + "','" + subdivision.Replace("$", "&") + "',@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][6].ToString());
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][10].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][11].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][12].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseSectionHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace("$", "&") + "','" + subdivision.Replace("$", "&") + "','" + jen.Replace("$", "&") + "',@Feeders=1,@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][14].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][15].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace("$", "&") + "','" + subdivision.Replace("$", "&") + "','" + jen.Replace("$", "&") + "',@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][7].ToString());
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][11].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][12].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "DB_View_MRPT_DRVDEnergyBalance_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace("$", "&") + "','" + subdivision.Replace("$", "&") + "','" + jen.Replace("$", "&") + "','" + substation.Replace("$", "&") + "',@Feeders=1,@MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][14].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][15].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }
        public List<sp_tripppings> GetTrippingAllData(string userid, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA @UserID='" + userid + "',@Optionalparam='ALL',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][2].ToString());
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][3].ToString());
                            trippings.Lessthan3Trippings = Convert.ToInt32(ds.Tables[0].Rows[i][4].ToString());
                            trippings.AverageTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][5].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingZonewiseHistoryData(string userid, string zone, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA  @UserID='" + userid + "',@Zone='" + zone + "',@Feeders=1,@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.Substation = ds.Tables[0].Rows[i][6].ToString();
                            trippings.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            trippings.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            trippings.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][10].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingCircleData(string userid, string zone, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA '" + userid + "','" + zone + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][3].ToString());
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][4].ToString());
                            trippings.Lessthan3Trippings = Convert.ToInt32(ds.Tables[0].Rows[i][5].ToString());
                            trippings.AverageTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][6].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingCirclewiseHistoryData(string userid, string zone, string circle, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA  @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Feeders=1,@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.Substation = ds.Tables[0].Rows[i][6].ToString();
                            trippings.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            trippings.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            trippings.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][10].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingDivisionData(string userid, string zone, string circle, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA '" + userid + "','" + zone + "','" + circle + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][4].ToString());
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][5].ToString());
                            trippings.Lessthan3Trippings = Convert.ToInt32(ds.Tables[0].Rows[i][6].ToString());
                            trippings.AverageTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][7].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingDivisionwiseHistoryData(string userid, string zone, string circle, string division, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@Feeders=1,@MonthYear = '" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.Substation = ds.Tables[0].Rows[i][6].ToString();
                            trippings.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            trippings.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            trippings.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][10].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingSubDivisionData(string userid, string zone, string circle, string division, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][5].ToString());
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][6].ToString());
                            trippings.Lessthan3Trippings = Convert.ToInt32(ds.Tables[0].Rows[i][7].ToString());
                            trippings.AverageTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][8].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingSubDivwiseHistoryData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@SubDivision='" + subdivision.Replace('$', '&') + "',@Feeders=1,@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.Substation = ds.Tables[0].Rows[i][6].ToString();
                            trippings.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            trippings.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            trippings.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][10].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "','" + subdivision.Replace('$', '&') + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][6].ToString());
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][7].ToString());
                            trippings.Lessthan3Trippings = Convert.ToInt32(ds.Tables[0].Rows[i][8].ToString());
                            trippings.AverageTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][9].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingSectionHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "','" + subdivision.Replace('$', '&') + "','" + jen.Replace('$', '&') + "',@Feeders=1,@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.Substation = ds.Tables[0].Rows[i][6].ToString();
                            trippings.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            trippings.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            trippings.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][10].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "','" + subdivision.Replace('$', '&') + "','" + jen.Replace('$', '&') + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.Substation = ds.Tables[0].Rows[i][6].ToString();
                            trippings.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][7].ToString());
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][8].ToString());
                            trippings.Lessthan3Trippings = Convert.ToInt32(ds.Tables[0].Rows[i][9].ToString());
                            trippings.AverageTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][10].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_tripppings> GetTrippingSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear, string type)
        {
            List<sp_tripppings> trippingslist = new List<sp_tripppings>();
            ds = new DataSet();
            query = "DB_View_MRPT_TrippingSummary_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "','" + subdivision.Replace('$', '&') + "','" + jen.Replace('$', '&') + "','" + substation.Replace('$', '&') + "',@Feeders=1,@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_tripppings trippings = new sp_tripppings();
                            trippings.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            trippings.Zone = ds.Tables[0].Rows[i][1].ToString();
                            trippings.Circle = ds.Tables[0].Rows[i][2].ToString();
                            trippings.Division = ds.Tables[0].Rows[i][3].ToString();
                            trippings.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            trippings.Section = ds.Tables[0].Rows[i][5].ToString();
                            trippings.Substation = ds.Tables[0].Rows[i][6].ToString();
                            trippings.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            trippings.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            trippings.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            trippings.TotalTrippings = Convert.ToInt32(ds.Tables[0].Rows[i][10].ToString());
                            trippingslist.Add(trippings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingslist.ToList();
        }
        public List<sp_distributionlosses_drawl> GetDLDrawlandSaleAllData(string userid, string monthyear, string type)
        {
            List<sp_distributionlosses_drawl> dldrawllist = new List<sp_distributionlosses_drawl>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLosess_skipMLA @UserID='" + userid + "',@Optionalparam='ALL',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_distributionlosses_drawl dldrawl = new sp_distributionlosses_drawl();
                            dldrawl.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dldrawl.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dldrawl.TED_FortheMonth = float.Parse(ds.Tables[0].Rows[i][2] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][2].ToString());
                            dldrawl.TED_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][3] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][3].ToString());
                            dldrawl.TES_FortheMonth = float.Parse(ds.Tables[0].Rows[i][4] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][4].ToString());
                            dldrawl.TES_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            dldrawl.AED_DuringtheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dldrawl.AED_UptotheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dldrawl.AES_DuringtheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dldrawl.AES_UptotheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dldrawl.EnergyDrawlVariation = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dldrawl.EnergySoldVariation = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dldrawllist.Add(dldrawl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dldrawllist.ToList();
        }
        public List<sp_distributionlosses_drawl> GetDLDrawlandSaleCircleData(string userid, string zone, string monthyear, string type)
        {
            List<sp_distributionlosses_drawl> dldrawllist = new List<sp_distributionlosses_drawl>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLosess_skipMLA '" + userid + "','" + zone + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_distributionlosses_drawl dldrawl = new sp_distributionlosses_drawl();
                            dldrawl.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dldrawl.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dldrawl.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dldrawl.TED_FortheMonth = float.Parse(ds.Tables[0].Rows[i][3] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][3].ToString());
                            dldrawl.TED_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][4] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][4].ToString());
                            dldrawl.TES_FortheMonth = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            dldrawl.TES_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dldrawl.AED_DuringtheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dldrawl.AED_UptotheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dldrawl.AES_DuringtheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dldrawl.AES_UptotheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dldrawl.EnergyDrawlVariation = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dldrawl.EnergySoldVariation = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dldrawllist.Add(dldrawl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dldrawllist.ToList();
        }
        public List<sp_distributionlosses_drawl> GetDLDrawlandSaleDivisionData(string userid, string zone, string circle, string monthyear, string type)
        {
            List<sp_distributionlosses_drawl> dldrawllist = new List<sp_distributionlosses_drawl>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLosess_skipMLA '" + userid + "','" + zone + "','" + circle + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_distributionlosses_drawl dldrawl = new sp_distributionlosses_drawl();
                            dldrawl.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dldrawl.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dldrawl.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dldrawl.Division = ds.Tables[0].Rows[i][3].ToString();
                            dldrawl.TED_FortheMonth = float.Parse(ds.Tables[0].Rows[i][4] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][4].ToString());
                            dldrawl.TED_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            dldrawl.TES_FortheMonth = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dldrawl.TES_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dldrawl.AED_DuringtheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dldrawl.AED_UptotheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dldrawl.AES_DuringtheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dldrawl.AES_UptotheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dldrawl.EnergyDrawlVariation = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dldrawl.EnergySoldVariation = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dldrawllist.Add(dldrawl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dldrawllist.ToList();
        }
        public List<sp_distributionlosses_drawl> GetDLDrawlandSaleSubDivisionData(string userid, string zone, string circle, string division, string monthyear, string type)
        {
            List<sp_distributionlosses_drawl> dldrawllist = new List<sp_distributionlosses_drawl>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLosess_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_distributionlosses_drawl dldrawl = new sp_distributionlosses_drawl();
                            dldrawl.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dldrawl.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dldrawl.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dldrawl.Division = ds.Tables[0].Rows[i][3].ToString();
                            dldrawl.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dldrawl.TED_FortheMonth = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            dldrawl.TED_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dldrawl.TES_FortheMonth = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dldrawl.TES_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dldrawl.AED_DuringtheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dldrawl.AED_UptotheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dldrawl.AES_DuringtheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dldrawl.AES_UptotheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dldrawl.EnergyDrawlVariation = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dldrawl.EnergySoldVariation = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dldrawllist.Add(dldrawl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dldrawllist.ToList();
        }
        public List<sp_distributionlosses_drawl> GetDLDrawlandSaleSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type)
        {
            List<sp_distributionlosses_drawl> dldrawllist = new List<sp_distributionlosses_drawl>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLosess_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "','" + subdivision.Replace('$', '&') + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_distributionlosses_drawl dldrawl = new sp_distributionlosses_drawl();
                            dldrawl.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dldrawl.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dldrawl.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dldrawl.Division = ds.Tables[0].Rows[i][3].ToString();
                            dldrawl.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dldrawl.Section = ds.Tables[0].Rows[i][5].ToString();
                            dldrawl.TED_FortheMonth = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dldrawl.TED_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dldrawl.TES_FortheMonth = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dldrawl.TES_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dldrawl.AED_DuringtheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dldrawl.AED_UptotheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dldrawl.AES_DuringtheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dldrawl.AES_UptotheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dldrawl.EnergyDrawlVariation = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dldrawl.EnergySoldVariation = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dldrawllist.Add(dldrawl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dldrawllist.ToList();
        }
        public List<sp_distributionlosses_drawl> GetDLDrawlandSaleSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type)
        {
            List<sp_distributionlosses_drawl> dldrawllist = new List<sp_distributionlosses_drawl>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLosess_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "','" + subdivision.Replace('$', '&') + "','" + jen.Replace('$', '&') + "',@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_drawl dldrawl = new sp_distributionlosses_drawl();
                            dldrawl.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dldrawl.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dldrawl.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dldrawl.Division = ds.Tables[0].Rows[i][3].ToString();
                            dldrawl.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dldrawl.Section = ds.Tables[0].Rows[i][5].ToString();
                            dldrawl.SubStation = ds.Tables[0].Rows[i][6].ToString();
                            dldrawl.TED_FortheMonth = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dldrawl.TED_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dldrawl.TES_FortheMonth = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dldrawl.TES_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dldrawl.AED_DuringtheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dldrawl.AED_UptotheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dldrawl.AES_DuringtheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dldrawl.AES_UptotheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dldrawl.EnergyDrawlVariation = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dldrawl.EnergySoldVariation = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dldrawllist.Add(dldrawl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dldrawllist.ToList();
        }
        public List<sp_distributionlosses_drawl> GetDLDrawlandSaleSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear, string type)
        {
            List<sp_distributionlosses_drawl> dldrawllist = new List<sp_distributionlosses_drawl>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLosess_skipMLA '" + userid + "','" + zone + "','" + circle + "','" + division.Replace('$', '&') + "','" + subdivision.Replace('$', '&') + "','" + jen.Replace('$', '&') + "','" + substation.Replace('$', '&') + "',@Feeders=1,@MonthYear='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_distributionlosses_drawl dldrawl = new sp_distributionlosses_drawl();
                            dldrawl.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dldrawl.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dldrawl.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dldrawl.Division = ds.Tables[0].Rows[i][3].ToString();
                            dldrawl.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dldrawl.Section = ds.Tables[0].Rows[i][5].ToString();
                            dldrawl.SubStation = ds.Tables[0].Rows[i][6].ToString();
                            dldrawl.TED_FortheMonth = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dldrawl.TED_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dldrawl.TES_FortheMonth = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dldrawl.TES_UptotheMonth = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dldrawl.AED_DuringtheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dldrawl.AED_UptotheMonth_kWh = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dldrawl.AES_DuringtheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dldrawl.AES_UptotheMonth_BilledEnergy = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dldrawl.EnergyDrawlVariation = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dldrawl.EnergySoldVariation = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dldrawllist.Add(dldrawl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dldrawllist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionAllData(string userid, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Summary='ALL',@FeedersLink=0,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][2].ToString());
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][3] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][3].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][4] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][4].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionZonewiseHistoryData(string userid, string zone, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@FeedersLink=1,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.Substation = ds.Tables[0].Rows[i][6].ToString();
                            dlanalst.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            dlanalst.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            dlanalst.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][17] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][17].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][18] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][18].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionCircleData(string userid, string zone, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@FeedersLink=0,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][3].ToString());
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][4] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][4].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionCirclewiseHistoryData(string userid, string zone, string circle, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@FeedersLink=1,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.Substation = ds.Tables[0].Rows[i][6].ToString();
                            dlanalst.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            dlanalst.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            dlanalst.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][17] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][17].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][18] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][18].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionDivisionData(string userid, string zone, string circle, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@FeedersLink=0,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][4].ToString());
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][5] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][5].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionDivisionwiseHistoryData(string userid, string zone, string circle, string division, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@FeedersLink=1,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.Substation = ds.Tables[0].Rows[i][6].ToString();
                            dlanalst.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            dlanalst.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            dlanalst.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][17] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][17].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][18] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][18].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionSubDivisionData(string userid, string zone, string circle, string division, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@FeedersLink=0,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][5].ToString());
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][6] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][6].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionSubDivwiseHistoryData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division + "',@SubDivision='" + subdivision + "',@FeedersLink=1,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.Substation = ds.Tables[0].Rows[i][6].ToString();
                            dlanalst.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            dlanalst.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            dlanalst.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][17] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][17].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][18] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][18].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@SubDivision='" + subdivision.Replace('$', '&') + "',@FeedersLink=0,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][6].ToString());
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][7] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][7].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionSectionHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@SubDivision='" + subdivision.Replace('$', '&') + "',@Section='" + jen.Replace('$', '&') + "',@FeedersLink=1,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.Substation = ds.Tables[0].Rows[i][6].ToString();
                            dlanalst.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            dlanalst.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            dlanalst.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][17] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][17].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][18] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][18].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@SubDivision='" + subdivision.Replace('$', '&') + "',@Section='" + jen.Replace('$', '&') + "',@FeedersLink=0,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.Substation = ds.Tables[0].Rows[i][6].ToString();
                            dlanalst.FeederNos = Convert.ToInt32(ds.Tables[0].Rows[i][7].ToString());
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][8] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][8].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][9] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][9].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_distributionlosses_analysis> GetDistributionSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear, string type)
        {
            List<sp_distributionlosses_analysis> dlanalstlist = new List<sp_distributionlosses_analysis>();
            ds = new DataSet();
            query = "DB_View_MRPT_DistributionLossAnalysis @UserID='" + userid + "',@Zone='" + zone + "',@Circle='" + circle + "',@Division='" + division.Replace('$', '&') + "',@SubDivision='" + subdivision.Replace('$', '&') + "',@Section='" + jen.Replace('$', '&') + "',@SubStation='" + substation.Replace('$', '&') + "',@FeedersLink=1,@BillMonth='" + monthyear + "',@FeederType='" + type + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_distributionlosses_analysis dlanalst = new sp_distributionlosses_analysis();
                            dlanalst.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            dlanalst.Zone = ds.Tables[0].Rows[i][1].ToString();
                            dlanalst.Circle = ds.Tables[0].Rows[i][2].ToString();
                            dlanalst.Division = ds.Tables[0].Rows[i][3].ToString();
                            dlanalst.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            dlanalst.Section = ds.Tables[0].Rows[i][5].ToString();
                            dlanalst.Substation = ds.Tables[0].Rows[i][6].ToString();
                            dlanalst.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            dlanalst.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            dlanalst.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            dlanalst.DTM_ED = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            dlanalst.DTM_ES = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            dlanalst.UTM_ED = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            dlanalst.UTM_ES = float.Parse(ds.Tables[0].Rows[i][13] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][13].ToString());
                            dlanalst.DTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][14] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][14].ToString());
                            dlanalst.UTM_TDLoss = float.Parse(ds.Tables[0].Rows[i][15] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][15].ToString());
                            dlanalst.UTM_MPWing = float.Parse(ds.Tables[0].Rows[i][16] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][16].ToString());
                            dlanalst.AsPerMPWing = float.Parse(ds.Tables[0].Rows[i][17] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][17].ToString());
                            dlanalst.AsPerOMWing = float.Parse(ds.Tables[0].Rows[i][18] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][18].ToString());
                            dlanalstlist.Add(dlanalst);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dlanalstlist.ToList();
        }
        public List<sp_qualitypower> GetQualityPowerSupplyData(string monthyear)
        {
            List<sp_qualitypower> qualitylist = new List<sp_qualitypower>();
            ds = new DataSet();
            query = "DB_View_QualityOfPowerSupply @MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_qualitypower quality = new sp_qualitypower();
                            quality.Sno = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            quality.Circle = ds.Tables[0].Rows[i][1].ToString();
                            quality.BlockFeeders = ds.Tables[0].Rows[i][2].ToString();
                            quality.NonBlockFeeders = ds.Tables[0].Rows[i][3].ToString();
                            quality.IndustrialFeeders = ds.Tables[0].Rows[i][4].ToString();
                            quality.WWFeeders = ds.Tables[0].Rows[i][5].ToString();
                            quality.TotalNos = ds.Tables[0].Rows[i][6].ToString();
                            quality.DLSupplyNos = ds.Tables[0].Rows[i][7].ToString();
                            quality.Per_DLSupply = ds.Tables[0].Rows[i][8].ToString();
                            quality.AGSupplyNos = ds.Tables[0].Rows[i][9].ToString();
                            quality.Per_AGSupply = ds.Tables[0].Rows[i][10].ToString();
                            qualitylist.Add(quality);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return qualitylist.ToList();
        }
        public List<sp_lrpmeasures> GetLRPMeasuresActionData(string monthyear)
        {
            List<sp_lrpmeasures> lrplist = new List<sp_lrpmeasures>();
            ds = new DataSet();
            query = "DB_View_ExessQualityOfPowerSupply @MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_lrpmeasures lrp = new sp_lrpmeasures();
                            lrp.Sno = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            lrp.Circle = ds.Tables[0].Rows[i][1].ToString();
                            lrp.TotalBlockFeeders = ds.Tables[0].Rows[i][2].ToString();
                            lrp.PrevMonthViolations = ds.Tables[0].Rows[i][3].ToString();
                            lrp.RepMonthViolations = ds.Tables[0].Rows[i][4].ToString();
                            lrp.PrevMonthRPTViolations = ds.Tables[0].Rows[i][5].ToString();
                            lrp.RepMonthRPTViolations = ds.Tables[0].Rows[i][6].ToString();
                            lrp.Remarks = ds.Tables[0].Rows[i][7].ToString();
                            lrplist.Add(lrp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return lrplist.ToList();
        }
        public List<sp_sla> GetSLAData(string monthyear)
        {
            List<sp_sla> slalist = new List<sp_sla>();
            ds = new DataSet();
            query = "DB_View_SLAreport @MonthYear='" + monthyear + "',@Param=0";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_sla sla = new sp_sla();
                            sla.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            sla.Zone = ds.Tables[0].Rows[i][1].ToString();
                            sla.Circle = ds.Tables[0].Rows[i][2].ToString();
                            sla.Commissioned = ds.Tables[0].Rows[i][3].ToString();
                            sla.DataReceived = ds.Tables[0].Rows[i][4].ToString();
                            sla.DataNotReceived = ds.Tables[0].Rows[i][5].ToString();
                            sla.SLA = ds.Tables[0].Rows[i][6].ToString();
                            slalist.Add(sla);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return slalist.ToList();
        }
        public List<sp_sla> GetSLAHistoryData(string monthyear, string parm, string zone, string circle)
        {
            List<sp_sla> slalist = new List<sp_sla>();
            ds = new DataSet();
            query = "DB_View_SLAreport @MonthYear='" + monthyear + "',@Param='" + parm + "',@Zone='" + zone + "',@Circle='" + circle + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_sla sla = new sp_sla();
                            sla.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            sla.Zone = ds.Tables[0].Rows[i][1].ToString();
                            sla.Circle = ds.Tables[0].Rows[i][2].ToString();
                            sla.Division = ds.Tables[0].Rows[i][3].ToString();
                            sla.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            sla.SubStation = ds.Tables[0].Rows[i][5].ToString();
                            sla.MeterNo = ds.Tables[0].Rows[i][6].ToString();
                            sla.FeederNo = ds.Tables[0].Rows[i][7].ToString();
                            sla.FeederName = ds.Tables[0].Rows[i][8].ToString();
                            sla.AMRPh = ds.Tables[0].Rows[i][9].ToString();
                            sla.ModemSno = ds.Tables[0].Rows[i][10].ToString();
                            sla.Remarks = ds.Tables[0].Rows[i][11].ToString();
                            slalist.Add(sla);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return slalist.ToList();
        }
        public List<sp_install> GetFeederInstallationData()
        {
            List<sp_install> installlist = new List<sp_install>();
            ds = new DataSet();
            query = "RPT_DaywiseInstallationStatus";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sp_install install = new sp_install();
                            install.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            install.Zone = ds.Tables[0].Rows[i][2].ToString();
                            install.Circle = ds.Tables[0].Rows[i][1].ToString();
                            install.RAPDRP = ds.Tables[0].Rows[i][3].ToString();
                            install.NONRAPDRP = ds.Tables[0].Rows[i][4].ToString();
                            install.Total = ds.Tables[0].Rows[i][5].ToString();
                            install.InstallationsDone = ds.Tables[0].Rows[i][6].ToString();
                            install.Registered = ds.Tables[0].Rows[i][7].ToString();
                            install.NotRegistered = ds.Tables[0].Rows[i][8].ToString();
                            install.DataReceived = ds.Tables[0].Rows[i][9].ToString();
                            install.DataNotReceived = ds.Tables[0].Rows[i][10].ToString();
                            install.Balance = ds.Tables[0].Rows[i][11].ToString();
                            install.PendingPercentage = ds.Tables[0].Rows[i][12].ToString();
                            installlist.Add(install);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return installlist.ToList();
        }
        public List<sp_feederwiseenergy> GetFeederwiseAuditSummaryData(string monthyear)
        {
            List<sp_feederwiseenergy> feederwisenergylist = new List<sp_feederwiseenergy>();
            ds = new DataSet();
            query = "EnergyAuditfor54Feeders @MonthYear='" + monthyear + "'";
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            sp_feederwiseenergy feederwisenergy = new sp_feederwiseenergy();
                            feederwisenergy.SNo = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                            feederwisenergy.Zone = ds.Tables[0].Rows[i][1].ToString();
                            feederwisenergy.Circle = ds.Tables[0].Rows[i][2].ToString();
                            feederwisenergy.Division = ds.Tables[0].Rows[i][3].ToString();
                            feederwisenergy.SubDivision = ds.Tables[0].Rows[i][4].ToString();
                            feederwisenergy.Section = ds.Tables[0].Rows[i][5].ToString();
                            feederwisenergy.Substation = ds.Tables[0].Rows[i][6].ToString();
                            feederwisenergy.FeederName = ds.Tables[0].Rows[i][7].ToString();
                            feederwisenergy.FeederNo = ds.Tables[0].Rows[i][8].ToString();
                            feederwisenergy.MeterNo = ds.Tables[0].Rows[i][9].ToString();
                            feederwisenergy.TotalUnits = float.Parse(ds.Tables[0].Rows[i][10] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][10].ToString());
                            feederwisenergy.T1 = float.Parse(ds.Tables[0].Rows[i][11] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][11].ToString());
                            feederwisenergy.T2 = float.Parse(ds.Tables[0].Rows[i][12] == DBNull.Value ? "0" : ds.Tables[0].Rows[i][12].ToString());
                            feederwisenergy.BA = ds.Tables[0].Rows[i][13].ToString();
                            feederwisenergy.CA = ds.Tables[0].Rows[i][14].ToString();
                            feederwisenergy.ATCloss = ds.Tables[0].Rows[i][15].ToString();
                            feederwisenergylist.Add(feederwisenergy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return feederwisenergylist.ToList();
        }

        //Added by Chanikya
        public List<sp_smsStatus> smsStatus(string meterno)
        {
            List<sp_smsStatus> smsList = new List<sp_smsStatus>();
            ds = new DataSet();
            query = string.Format("SP_MOB_SMSRequest @Meterno='{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        sp_smsStatus sms = new sp_smsStatus();
                        sms.supplyStatus = ds.Tables[0].Rows[0][0].ToString();
                        sms.SMS_status = ds.Tables[0].Rows[0][1].ToString();
                        smsList.Add(sms);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return smsList.ToList();
        }

        public List<instants> getMeterInstants(string meterno)
        {
            List<instants> instantsList = new List<instants>();
            query = string.Format("DB_View_Feeder_Instants '{0}'", meterno);
            ds = new DataSet();
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            instants instant = new instants();
                            instant.vr = ds.Tables[0].Rows[0]["vr"].ToString();
                            instant.vy = ds.Tables[0].Rows[0]["vy"].ToString();
                            instant.vb = ds.Tables[0].Rows[0]["vb"].ToString();
                            instant.ir = ds.Tables[0].Rows[0]["ir"].ToString();
                            instant.iy = ds.Tables[0].Rows[0]["iy"].ToString();
                            instant.ib = ds.Tables[0].Rows[0]["ib"].ToString();
                            instant.qr = ds.Tables[0].Rows[0]["qr"].ToString();
                            instant.qy = ds.Tables[0].Rows[0]["qy"].ToString();
                            instant.qb = ds.Tables[0].Rows[0]["qb"].ToString();
                            instant.meter_date = ds.Tables[0].Rows[0]["mdate"].ToString();
                            instant.kw = ds.Tables[0].Rows[0]["kw"].ToString();
                            instant.kvar = ds.Tables[0].Rows[0]["kvar"].ToString();
                            instant.kva = ds.Tables[0].Rows[0]["kva"].ToString();
                            instant.qavg = ds.Tables[0].Rows[0]["qavg"].ToString();
                            instant.freq = ds.Tables[0].Rows[0]["freq"].ToString();
                            instant.cum_kwh = ds.Tables[0].Rows[0]["cumkWh"].ToString();
                            instant.cum_kvarh_lg = ds.Tables[0].Rows[0]["cumkVArh_Lag"].ToString();
                            instant.cum_kvarh_ld = ds.Tables[0].Rows[0]["cumkVArh_Lead"].ToString();
                            instant.cum_kvah = ds.Tables[0].Rows[0]["CumkVAh"].ToString();
                            instant.mdKW = ds.Tables[0].Rows[0]["MDkW"].ToString();
                            instant.mdkw_date = ds.Tables[0].Rows[0]["MDkWDate"].ToString();
                            instant.mdkva = ds.Tables[0].Rows[0]["MDkVA"].ToString();
                            instant.mdkva_date = ds.Tables[0].Rows[0]["MDkVADate"].ToString();
                            instant.powerFailCount = ds.Tables[0].Rows[0]["pwrfailcount"].ToString();
                            instant.cum_pwr_fail_dur = ds.Tables[0].Rows[0]["cumpwrfaildur"].ToString();
                            instant.bill_date = ds.Tables[0].Rows[0]["BillingDate"].ToString();
                            instant.cum_tmpr_count = ds.Tables[0].Rows[0]["cumtmprcount"].ToString();
                            instant.cum_prog_count = ds.Tables[0].Rows[0]["cumprogcount"].ToString();
                            instant.cum_bill_count = ds.Tables[0].Rows[0]["cumbillcount"].ToString();
                            instantsList.Add(instant);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return instantsList.ToList();
        }

        public List<monthlyGraph> getMonthlyGraphData(string meterno, string month, string status)
        {
            List<monthlyGraph> monthlyList = new List<monthlyGraph>();
            query = string.Format("DB_View_Feeder_AngularUTC '{0}','{1}'", meterno, month);
            ds = new DataSet();
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (status == "three")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                monthlyGraph monthly = new monthlyGraph();
                                monthly.th_Ph = ds.Tables[0].Rows[i][9].ToString();
                                monthly.th_ph_Dur = ds.Tables[0].Rows[i][14].ToString();
                                monthlyList.Add(monthly);
                            }
                        }

                        else if (status == "two")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                monthlyGraph monthly = new monthlyGraph();
                                monthly.tw_ph = ds.Tables[0].Rows[i][10].ToString();
                                monthly.tw_ph_Dur = ds.Tables[0].Rows[i][15].ToString();
                                monthlyList.Add(monthly);
                            }
                        }
                        else if (status == "one")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                monthlyGraph monthly = new monthlyGraph();
                                monthly.si_ph = ds.Tables[0].Rows[i][11].ToString();
                                monthly.si_ph_Dur = ds.Tables[0].Rows[i][16].ToString();
                                monthlyList.Add(monthly);
                            }
                        }
                        else if (status == "powerfail")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                monthlyGraph monthly = new monthlyGraph();
                                monthly.powerFail = ds.Tables[0].Rows[i][12].ToString();
                                monthly.powerFail_Dur = ds.Tables[0].Rows[i][17].ToString();
                                monthlyList.Add(monthly);
                            }
                        }
                        else if (status == "notcomm")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                monthlyGraph monthly = new monthlyGraph();
                                monthly.notCom = ds.Tables[0].Rows[i][13].ToString();
                                monthly.not_comm_Dur = ds.Tables[0].Rows[i][18].ToString();
                                monthlyList.Add(monthly);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return monthlyList.ToList();

        }

        public List<loadGraph> getLoadGraphData(string meterno, string status)
        {
            List<loadGraph> graphList = new List<loadGraph>();
            ds = new DataSet();
            query = string.Format("DB_View_Feeder_LoadGraph '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (status == "kw")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                loadGraph graph = new loadGraph();
                                graph.time = ds.Tables[0].Rows[i][0].ToString();
                                graph.kw = ds.Tables[0].Rows[i][1].ToString();
                                graphList.Add(graph);
                            }
                        }

                        else if (status == "kva")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                loadGraph graph = new loadGraph();
                                graph.time = ds.Tables[0].Rows[i][0].ToString();
                                graph.kva = ds.Tables[0].Rows[i][2].ToString();
                                graphList.Add(graph);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return graphList.ToList();
        }

        public List<energyGraph> getEnergyGrpahData(string meterno, string status)
        {
            List<energyGraph> energyList = new List<energyGraph>();
            ds = new DataSet();
            query = string.Format("DB_View_Feeder_EnergyGraph '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (status == "kwh")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                energyGraph energy = new energyGraph();
                                energy.time = ds.Tables[0].Rows[i][0].ToString();
                                energy.kwh = ds.Tables[0].Rows[i][1].ToString();
                                energyList.Add(energy);
                            }
                        }
                        else if (status == "kvah")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                energyGraph energy = new energyGraph();
                                energy.time = ds.Tables[0].Rows[i][0].ToString();
                                energy.kvah = ds.Tables[0].Rows[i][2].ToString();
                                energyList.Add(energy);
                            }
                        }
                        else if (status == "kvarh_lg")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                energyGraph energy = new energyGraph();
                                energy.time = ds.Tables[0].Rows[i][0].ToString();
                                energy.kvarh_lag = ds.Tables[0].Rows[i][3].ToString();
                                energyList.Add(energy);
                            }
                        }
                        else if (status == "kvarh_ld")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                energyGraph energy = new energyGraph();
                                energy.time = ds.Tables[0].Rows[i][0].ToString();
                                energy.kvarh_lead = ds.Tables[0].Rows[i][4].ToString();
                                energyList.Add(energy);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return energyList.ToList();
        }

        public List<powerGraph> getPowerGraphData(string meterno)
        {
            List<powerGraph> powerDetails = new List<powerGraph>();
            ds = new DataSet();
            query = string.Format("SP_DB_View_Graph '{0}','Power Factor'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            powerGraph power = new powerGraph();
                            power.time = ds.Tables[0].Rows[i][3].ToString();
                            power.pf = ds.Tables[0].Rows[i][4].ToString();
                            powerDetails.Add(power);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return powerDetails.ToList();

        }

        public List<currentGraph> getCurrentGraphData(string meterno, string status)
        {
            List<currentGraph> currentDetails = new List<currentGraph>();
            ds = new DataSet();
            query = string.Format("DB_View_Feeder_CurrentGraph '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (status == "ir")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                currentGraph current = new currentGraph();
                                current.time = ds.Tables[0].Rows[i][0].ToString();
                                current.Ir = ds.Tables[0].Rows[i][1].ToString();
                                currentDetails.Add(current);
                            }
                        }
                        else if (status == "iy")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                currentGraph current = new currentGraph();
                                current.time = ds.Tables[0].Rows[i][0].ToString();
                                current.Iy = ds.Tables[0].Rows[i][2].ToString();
                                currentDetails.Add(current);
                            }
                        }
                        else if (status == "ib")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                currentGraph current = new currentGraph();
                                current.time = ds.Tables[0].Rows[i][0].ToString();
                                current.Ib = ds.Tables[0].Rows[i][3].ToString();
                                currentDetails.Add(current);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }

            return currentDetails.ToList();

        }

        public List<voltageGraph> getVoltageGrpahData(string meterno, string status)
        {
            List<voltageGraph> voltageGrpahList = new List<voltageGraph>();
            ds = new DataSet();
            query = string.Format("DB_View_Feeder_VoltageGraph '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (status == "vr")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                voltageGraph graph = new voltageGraph();
                                graph.time = ds.Tables[0].Rows[i][0].ToString();
                                graph.vr = ds.Tables[0].Rows[i][1].ToString();
                                voltageGrpahList.Add(graph);
                            }
                        }
                        else if (status == "vy")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                voltageGraph graph = new voltageGraph();
                                graph.time = ds.Tables[0].Rows[i][0].ToString();
                                graph.vy = ds.Tables[0].Rows[i][2].ToString();
                                voltageGrpahList.Add(graph);
                            }
                        }
                        else if (status == "vb")
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                voltageGraph graph = new voltageGraph();
                                graph.time = ds.Tables[0].Rows[i][0].ToString();
                                graph.vb = ds.Tables[0].Rows[i][3].ToString();
                                voltageGrpahList.Add(graph);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }

            return voltageGrpahList.ToList();
        }

        public List<dailyLoad> getDailyLoad(string meterno)
        {
            List<dailyLoad> dailyLoadList = new List<dailyLoad>();
            ds = new DataSet();
            query = string.Format("DB_View_Feeder_DailyLoad  '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            dailyLoad dailyLoad = new dailyLoad();
                            dailyLoad.Sno = ds.Tables[0].Rows[i][0].ToString();
                            dailyLoad.Meterno = ds.Tables[0].Rows[i][1].ToString();
                            dailyLoad.mdate = ds.Tables[0].Rows[i][2].ToString();
                            dailyLoad.cumkwh_imp = ds.Tables[0].Rows[i][3].ToString();
                            dailyLoad.cumkwh_exp = ds.Tables[0].Rows[i][4].ToString();
                            dailyLoad.cumkvah_kwimp = ds.Tables[0].Rows[i][5].ToString();
                            dailyLoad.cumkvah_kwexp = ds.Tables[0].Rows[i][6].ToString();
                            dailyLoad.cumkvarh_q1 = ds.Tables[0].Rows[i][7].ToString();
                            dailyLoad.cumkvarh_q2 = ds.Tables[0].Rows[i][8].ToString();
                            dailyLoad.cumkvarh_q3 = ds.Tables[0].Rows[i][9].ToString();
                            dailyLoad.cumkvarh_q4 = ds.Tables[0].Rows[i][10].ToString();
                            dailyLoad.meterDate = ds.Tables[0].Rows[i][11].ToString();
                            dailyLoadList.Add(dailyLoad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dailyLoadList.ToList();
        }

        public List<supplyLog> getSupplyLogData(string meterno, string date)
        {
            List<supplyLog> supplyList = new List<supplyLog>();
            ds = new DataSet();
            query = string.Format("DB_View_MRPT_FeederSupplyDetails '{0}','{1}'", meterno, date);
            ds = dataclass.GetData(query);
            DataTable dt = new DataTable();

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            supplyLog supply = new supplyLog();
                            supply.Sno = dt.Rows[i]["SNo"].ToString();
                            supply.date = dt.Rows[i]["Dt"].ToString();
                            supply.threePhaseDuration = dt.Rows[i][2].ToString();
                            supply.threePhaseTimeDuration = dt.Rows[i][3].ToString();
                            supply.twoPhaseDuration = dt.Rows[i][4].ToString();
                            supply.twoPhaseTimeDuraton = dt.Rows[i][5].ToString();
                            supply.singlePhaseDuration = dt.Rows[i][6].ToString();
                            supply.singlePhaseTimeDuration = dt.Rows[i][7].ToString();
                            supply.ourtageDuration = dt.Rows[i][8].ToString();
                            supply.outageTimeDuration = dt.Rows[i][9].ToString();
                            supply.totalSupplyDuration = dt.Rows[i][10].ToString();
                            supply.remarks = dt.Rows[i][11].ToString();
                            supplyList.Add(supply);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return supplyList.ToList();
        }

        public List<trippingLog> getTrippingLogData(string meterno, string date)
        {
            List<trippingLog> trippingList = new List<trippingLog>();
            ds = new DataSet();
            query = string.Format("DB_View_MRPT_FeederTrippingDetails '{0}','{1}'", meterno, date);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            trippingLog tripping = new trippingLog();
                            tripping.date = ds.Tables[0].Rows[i][1].ToString(); ;
                            tripping.th_ph_tripping = ds.Tables[0].Rows[i][2].ToString();
                            tripping.th_ph_tripping_l10 = ds.Tables[0].Rows[i][3].ToString();
                            tripping.tw_ph_tripping = ds.Tables[0].Rows[i][4].ToString();
                            tripping.tw_ph_tripping_l10 = ds.Tables[0].Rows[i][5].ToString();
                            tripping.si_ph_tripping = ds.Tables[0].Rows[i][6].ToString();
                            tripping.si_ph_tripping_l10 = ds.Tables[0].Rows[i][7].ToString();
                            tripping.tot_ph_tripping = ds.Tables[0].Rows[i][8].ToString();
                            tripping.tot_ph_tripping_l10 = ds.Tables[0].Rows[i][9].ToString();
                            tripping.remarks = ds.Tables[0].Rows[i][10].ToString();
                            trippingList.Add(tripping);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return trippingList.ToList();
        }

        public List<mergegrid> getMergeData(string meterno, string date)
        {
            List<mergegrid> mergeList = new List<mergegrid>();
            ds = new DataSet();
            query = string.Format("SP_DB_View_LogBookConsumption '{0}','{1}'", meterno, date);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            mergegrid merge = new mergegrid();
                            merge.tr_ph_con = ds.Tables[0].Rows[i][1].ToString();
                            merge.tw_ph_con = ds.Tables[0].Rows[i][2].ToString();
                            merge.si_ph_con = ds.Tables[0].Rows[i][3].ToString();
                            mergeList.Add(merge);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return mergeList.ToList();
        }

        public List<loadSurvey> getLoadSurvey(string meterno)
        {
            List<loadSurvey> surveyList = new List<loadSurvey>();
            ds = new DataSet();
            query = string.Format("DB_View_Feeder_LoadSurvey '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            loadSurvey survey = new loadSurvey();
                            survey.Sno = ds.Tables[0].Rows[i][0].ToString();
                            survey.Meterno = ds.Tables[0].Rows[i][1].ToString();
                            survey.mdate = ds.Tables[0].Rows[i][2].ToString();
                            survey.vr = ds.Tables[0].Rows[i][3].ToString();
                            survey.vy = ds.Tables[0].Rows[i][4].ToString();
                            survey.vb = ds.Tables[0].Rows[i][5].ToString();
                            survey.ir = ds.Tables[0].Rows[i][6].ToString();
                            survey.iy = ds.Tables[0].Rows[i][7].ToString();
                            survey.ib = ds.Tables[0].Rows[i][8].ToString();
                            survey.kwh = ds.Tables[0].Rows[i][9].ToString();
                            survey.kvarh_lg = ds.Tables[0].Rows[i][10].ToString();
                            survey.kvarh_ld = ds.Tables[0].Rows[i][11].ToString();
                            survey.kvarh = ds.Tables[0].Rows[i][12].ToString();
                            survey.freq = ds.Tables[0].Rows[i][13].ToString();
                            survey.meterDate = ds.Tables[0].Rows[i][14].ToString();

                            surveyList.Add(survey);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return surveyList.ToList();
        }

        public List<meterEvents> getMeterEvents(string meterno)
        {
            List<meterEvents> eventsList = new List<meterEvents>();
            ds = new DataSet();
            query = string.Format("DB_View_Feeder_MeterEvents '{0}'", meterno);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            meterEvents events = new meterEvents();
                            events.Sno = ds.Tables[0].Rows[i][0].ToString();
                            events.meterno = ds.Tables[0].Rows[i][1].ToString();
                            events.edate = ds.Tables[0].Rows[i][2].ToString();
                            events.status = ds.Tables[0].Rows[i][3].ToString();
                            events.vr = ds.Tables[0].Rows[i][4].ToString();
                            events.vy = ds.Tables[0].Rows[i][5].ToString();
                            events.vb = ds.Tables[0].Rows[i][6].ToString();
                            events.vry = ds.Tables[0].Rows[i][7].ToString();
                            events.vyb = ds.Tables[0].Rows[i][8].ToString();
                            events.ir = ds.Tables[0].Rows[i][9].ToString();
                            events.iy = ds.Tables[0].Rows[i][10].ToString();
                            events.ib = ds.Tables[0].Rows[i][11].ToString();
                            events.qr = ds.Tables[0].Rows[i][12].ToString();
                            events.qy = ds.Tables[0].Rows[i][13].ToString();
                            events.qb = ds.Tables[0].Rows[i][14].ToString();
                            events.cum_kWh = ds.Tables[0].Rows[i][15].ToString();
                            eventsList.Add(events);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return eventsList.ToList();
        }

        public List<feederSummary> getFeederSummaryDetails(string meterno, string sdate, string edate)
        {
            List<feederSummary> getfeederList = new List<feederSummary>();
            ds = new DataSet();
            query = string.Format("DB_View_FeederSummary '{0}','{1}','{2}'", meterno, sdate, edate);
            ds = dataclass.GetData(query);
            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            feederSummary feeder = new feederSummary();
                            feeder.maxMDKw = ds.Tables[0].Rows[0]["MaxMDkW"].ToString();
                            feeder.MinMDKw = ds.Tables[0].Rows[0]["MinMDkW"].ToString();
                            feeder.AvgMdKW = ds.Tables[0].Rows[0]["AvgMDkW"].ToString();
                            feeder.kWh = ds.Tables[0].Rows[0]["kWh"].ToString();
                            feeder.kVAh = ds.Tables[0].Rows[0]["kVAh"].ToString();
                            feeder.kVArh_lag = ds.Tables[0].Rows[0]["kVArh_lag"].ToString();
                            feeder.kVArh_lead = ds.Tables[0].Rows[0]["kVArh_Lead"].ToString();
                            feeder.maxPF = ds.Tables[0].Rows[0]["MaxPF"].ToString();
                            feeder.minPF = ds.Tables[0].Rows[0]["MinPF"].ToString();
                            feeder.avgPF = ds.Tables[0].Rows[0]["AvgPF"].ToString();
                            feeder.minMDkVA = ds.Tables[0].Rows[0]["MaxMDkVA"].ToString();
                            feeder.AvgMDkVA = ds.Tables[0].Rows[0]["MinMDkVA"].ToString();
                            feeder.MaxMDkVA = ds.Tables[0].Rows[0]["AvgMDkVA"].ToString();
                            feeder.MaxVolt = ds.Tables[0].Rows[0]["MaxVolt"].ToString();
                            feeder.MinVolt = ds.Tables[0].Rows[0]["MinVolt"].ToString();
                            feeder.AvgVolt = ds.Tables[0].Rows[0]["AvgVolt"].ToString();
                            feeder.MaxCur = ds.Tables[0].Rows[0]["MaxCur"].ToString();
                            feeder.MinCur = ds.Tables[0].Rows[0]["MinCur"].ToString();
                            feeder.AvgCur = ds.Tables[0].Rows[0]["AvgCur"].ToString();
                            feeder.TotalInt = ds.Tables[0].Rows[0]["TotalInt"].ToString();
                            feeder.TotalIntDur = ds.Tables[0].Rows[0]["TotalIntDur"].ToString();
                            feeder.HighDur = ds.Tables[0].Rows[0]["HighestIntDur"].ToString();
                            feeder.lowDur = ds.Tables[0].Rows[0]["LowestIntDur"].ToString();
                            feeder.AG_Cons = ds.Tables[0].Rows[0]["ag_consumption"].ToString();
                            feeder.DL_Cons = ds.Tables[0].Rows[0]["dl_consumption"].ToString();
                            feeder.Three_ph = ds.Tables[0].Rows[0]["ThreePhCons"].ToString();
                            feeder.Single_ph = ds.Tables[0].Rows[0]["SinglePhCons"].ToString();

                            getfeederList.Add(feeder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return getfeederList.ToList();
        }

        public bool changePWD(string userid, string username, string deignetion, string oldpwd, string newpwd, string cityname, string ipAddress, string zipCode)
        {
            query = string.Format("SP_UserActivityLog @UserId='{0}',@UserName='{1}',@Designation='{2}',@OldPwd='{3}',@Domain='{4}',@ServerName='{5}',@ServerIp='{6}',@Acitivity='Change Password', @NewPwd='{7}'", userid, username, deignetion, oldpwd, zipCode, cityname, ipAddress, newpwd);
            if (dataclass.getQueryResult(query) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
