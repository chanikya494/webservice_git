using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.ServiceModel;

public class DatabaseAccess
{
    public SqlConnection Conn = new SqlConnection();
    SqlCommand cmd;
    SqlDataAdapter adpt;
    DataSet result_ds=new DataSet();
    DataSet data_ds=new DataSet();
    DataSet result_combods=new DataSet();
    DataSet result_listviewds=new DataSet();
    Object result;
    object update_insert_delete_lockobj = new object();
    string dateformat = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
    string encrstr;
    int i = 0;
    public string StrStarttdate = string.Empty;
    public string StrEnddate = string.Empty;
    public string StrMeterValue = string.Empty;
    List<string> meters = new List<string>();
    DataTable dt;
    public List<string> sub_meters = new List<string>();
    Dictionary<string, string> dct = new Dictionary<string, string>();
   

    public SqlConnection GetConnection()
    {
       
            Conn.ConnectionString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            return Conn;
        

    }
    public string PasswordEncryption(string data)
    {
        try
        {
            //PASSWORD ENCRYPTION
            string sValue = "";
            byte  byte_= 0;
            foreach (char c in data)
            {
                byte_=Convert.ToByte(c);
                sValue += byte_.ToString("x").ToUpper();
            }
            return sValue;
        }
        catch (Exception ex)
        {
           
        }
        return null;
    }
    public object PasswordDecryption(string data)
    {
        try
        {
            // PASSWORD DECRYPTION
            string data1 = string.Empty;
            string sdata = string.Empty;
            while ((data.Length > 0))
            {
                data1 = System.Convert.ToChar(System.Convert.ToUInt32(data.Substring(0, 2), 16)).ToString();
                sdata += data1;
                data = data.Substring(2, data.Length - 2);
            }
            encrstr = sdata;
            return sdata;
        }
        catch (Exception ex)
        {
           
        }
        return null;
    }
   
    public DataSet GetData(string qry)
    {
        try
        {
            if (Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                Conn = GetConnection();
                Conn.Open();
            }
            result_ds = new DataSet();
            adpt = new SqlDataAdapter(qry, Conn);
            adpt.SelectCommand.CommandTimeout = 120;
            adpt.Fill(result_ds);
            if (result_ds.Tables[0].Rows.Count > 0)
            {
                return result_ds.Copy();
            }
            else
            {
                return result_ds;
            }
        }
        catch (SqlException sqex)
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        catch (Exception ex)
        {
           
        }
        finally
        {
             Conn.Close();
             SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        return result_ds;
    }
    public DataSet GetData(string qry, SqlConnection mycon)
    {
        try
        {
            if (Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                Conn = GetConnection();
                Conn.Open();
            }
            result_ds = new DataSet();
            adpt = new SqlDataAdapter(qry, mycon);
            adpt.Fill(result_ds);
            if (result_ds.Tables[0].Rows.Count > 0)
            {
                return result_ds.Copy();
            }
            else
            {
                return result_ds;
            }
        }
        catch (SqlException Mysqex)
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        catch (Exception ex)
        {
        }
        finally
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        return result_ds;
    }
    public string GetRootData(int id)
    {
        try
        {
            if (Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                Conn = GetConnection();
                Conn.Open();
            }
            cmd = new SqlCommand("select menu_name from web_menus where pos_id=" + id, Conn);
            return Convert.ToString(cmd.ExecuteScalar());
        }
        catch (SqlException Mysqex)
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        catch (Exception ex)
        {

        }
         finally
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        return "";
    }
    public int delete_update_insert(string qry)
    {
       lock(update_insert_delete_lockobj)
       {
           try
           {
            result=0;
            if (Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                Conn = GetConnection();
                Conn.Open();
            }
                cmd = new SqlCommand(qry, Conn);
                result = cmd.ExecuteNonQuery();
                System.Threading.Thread.Sleep(100);
           }
           catch(SqlException  Mysqex)
           {
                Conn.Close();
                SqlConnection.ClearPool(Conn);
                Conn.Dispose();
           }
           catch(Exception ex)
           {

           }
           finally
           {
                Conn.Close();
                SqlConnection.ClearPool(Conn);
                Conn.Dispose();
           }
           return (int)result;
       }
    }
    public Object GetCount(string qry)
    {
        try
        {
            if(Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                  Conn = GetConnection();
                  Conn.Open();
            }
            cmd = new SqlCommand(qry, Conn);
            result=cmd.ExecuteScalar();
        }
        catch (SqlException Mysqex)
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        catch (Exception ex)
        {

        }
        finally 
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
             Conn.Dispose();
        }
         return result;
    }

    public int getQueryResult(string query)
    {
        int count = 0;
        try
        {
            if (Conn.State != ConnectionState.Open)
            {
                Conn = GetConnection();
                Conn.Open();
            }

            SqlCommand cmd = new SqlCommand(query, Conn);
            count = cmd.ExecuteNonQuery();
            return count;
        }

        catch (Exception ex)
        {
            return count;
        }

    }

    public DropDownList FillCombo(string qry,DropDownList dropdown,string displaymember)
    {
        try
        {
            if(Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                  Conn = GetConnection();
                  Conn.Open();
            }
            result_combods.Clear();
            dropdown.Items.Clear();
             adpt = new SqlDataAdapter(qry, Conn);
             adpt.Fill(result_combods);
            if(result_combods.Tables[0].Rows.Count > 0)
            {
                for(i=0; i<=result_combods.Tables[0].Rows.Count -1;i++)
                {
                    dropdown.Items.Add(result_combods.Tables[0].Rows[i][displaymember].ToString());
                }
            }
        }
        catch (SqlException Mysqex)
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        catch (Exception ex)
        {
            WriteErrorLog(ex.Message);
        }
        finally 
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        return dropdown;
    }
    public void update_logout(string str_userlogindate, int int_userid, string str_useripaddress, string str_userhostname, string str_logoutreason)
    {
        try
        {
            delete_update_insert("update tblweb_user_log set user_logoutdatetime=to_date('" + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")) + "','dd-mm-yyyy hh24:mi:ss'),user_logout_reason='" + str_logoutreason + "' where user_logindatetime=to_date('" + str_userlogindate + "','dd-mm-yyyy hh24:mi:ss') and user_id=" + int_userid + " and user_ipaddress='" + str_useripaddress + "' and user_hostname='" + str_userhostname + "'");
        }
        catch (Exception ex)
        {

        }
    }

    public string GetColour(int num)
    {
        string colour = string.Empty;
        Random rand = new Random();
        int code = rand.Next(1, 10);
        if (num <= 9)
        {
            code = num;
        }
        else
        {
            code = num % 10;
        }
        switch (code)
        {
            case 1:
                colour = "color_a";
                break;
            case 2:
                colour = "color_b";
                break;
            case 3:
                colour = "color_c";
                break;
            case 4:
                colour = "color_d";
                break;
            case 5:
                colour = "color_e";
                break;
            case 6:
                colour = "color_f";
                break;
            case 7:
                colour = "color_g";
                break;
            case 8:
                colour = "color_h";
                break;
            case 9:
                colour = "color_i";
                break;
            case 10:
                colour = "color_j";
                break;

            default:
                colour = "white";
                break;
        }
        return colour;
    }
    public DataTable GetFeeder(string qry)
    {
        try
        {
            if (Conn.State == ConnectionState.Closed || Conn.State == ConnectionState.Broken)
            {
                Conn = GetConnection();
                Conn.Open();
            }
            adpt = new SqlDataAdapter(qry, Conn);
            dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return dt.Copy();
            }
            else
            {
                return dt;
            }
        }
        catch (SqlException Mysqex)
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        catch (Exception ex)
        {
        }
        finally
        {
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            Conn.Dispose();
        }
        return dt;
    }
    

    public void WriteErrorLog(string error)
    {
        string pageName = Path.GetFileName(HttpContext.Current.Request.Path);
        string filename = "Log_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string filepath = HttpContext.Current.Server.MapPath("~/logfiles/" + filename);
        if (File.Exists(filepath))
        {
            using (StreamWriter stwriter = new StreamWriter(filepath, true))
            {
                stwriter.WriteLine("-------------------START-------------" + DateTime.Now);
                stwriter.WriteLine("Page :" + pageName);
                stwriter.WriteLine(error);
                stwriter.WriteLine("-------------------END-------------" + DateTime.Now);
            }
        }
        else
        {
            StreamWriter stwriter = File.CreateText(filepath);
            stwriter.WriteLine("-------------------START-------------" + DateTime.Now);
            stwriter.WriteLine("Page :" + pageName);
            stwriter.WriteLine(error);
            stwriter.WriteLine("-------------------END-------------" + DateTime.Now);
            stwriter.Close();
        }
    }
   
   
}


