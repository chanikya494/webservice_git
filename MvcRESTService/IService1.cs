﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MvcRESTService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "login/{userid}/{password}")]
        List<userMaster> getUserName(string userid, string password);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "changePWD/{userid}/{username}/{deignetion}/{oldpwd}/{newpwd}/{cityname}/{ipAddress}/{zipCode}")]
        bool changePWD(string userid, string username, string deignetion, string oldpwd, string newpwd, string cityname, string ipAddress, string zipCode);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/ZoneData/{user}")]
        List<sp_zone_result> GetZoneData(string user);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/CircleData/{zone}/{user}")]
        List<sp_circle_result> GetCircleData(string zone, string user);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/MlaData/{zone}/{circle}/{user}")]
        List<sp_mla_result> GetMlaData(string zone, string circle, string user);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DivisionData/{zone}/{circle}/{mla}/{user}")]
        List<sp_division_result> GetDivisionData(string zone, string circle, string mla, string user);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/SubDivisionData/{zone}/{circle}/{mla}/{division}/{user}")]
        List<sp_subdiv_result> GetSubDivisionData(string zone, string circle, string mla, string division, string user);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/JenData/{zone}/{circle}/{mla}/{division}/{subdivision}/{user}")]
        List<sp_jen_result> GetJuniorEngineerData(string zone, string circle, string mla, string division, string subdivision, string user);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/PieData/{user}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}")]
        List<sp_piedata> GetPieData(string user, string zone, string circle, string mla, string division, string subdivision, string jen);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/SubstationData/{user}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}")]
        List<sp_substations> GetSubstations(string user, string zone, string circle, string mla, string division, string subdivision, string jen);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/PhaseData/{user}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{estatus}/{pdate}")]
        List<sp_phases> GetPhaseData(string user, string zone, string circle, string mla, string division, string subdivision, string jen, string estatus, string pdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/BlocakLoad/{meterno}")]
        List<sp_Feeder_loadSurvey> getFeederLoadSurvey(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/HalfHourPeakLoad/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{startdate}")]
        List<sp_HalfHourlyPeakload> getHalfHourlyPeaklod(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string startdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DaywiseSubstationPeakLoad/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{startdate}")]
        List<sp_daywisesubstationpeakload> getDaywiseSubstationPeakLoad(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string startdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/pfAbstract/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{pfdate}")]
        List<sp_PfAbstract> getPfAbstract(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string pfdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getOverlad/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{oudate}")]
        List<sp_overUnderload> getOverload(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string oudate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getfeederInterruption/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{itrdate}")]
        List<sp_feederInterruption> getFeederInterruption(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string itrdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getMeterInstants/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}")]
        List<sp_feederInstants> getFeederInstants(string userid, string zone, string circle, string mla, string division, string subdivision, string jen);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getFeederNotComm/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}")]
        List<sp_feederNotCommunicating> getFeederNotComm(string userid, string zone, string circle, string mla, string division, string subdivision, string jen);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getParent/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{logdate}")]
        List<sp_LogParent> getLogParent(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string logdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getChild/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{logdate}/{meterno}")]
        List<sp_LogChild> getLogChild(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string logdate, string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getFeederDetails/{meterno}")]
        List<feederDetails> getFeeder(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getFeederSummary/{meterno}/{sdate}/{edate}")]
        List<feederSummary> getFeederSummaryDetails(string meterno, string sdate, string edate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getMeterEvents/{meterno}")]
        List<meterEvents> getMeterEvents(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getLoadSurvey/{meterno}")]
        List<loadSurvey> getLoadSurvey(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getDailyLoad/{meterno}")]
        List<dailyLoad> getDailyLoad(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getMergeData/{meterno}/{date}")]
        List<mergegrid> getMergeData(string meterno, string date);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getSupplyLog/{meterno}/{date}")]
        List<supplyLog> getSupplyLogData(string meterno, string date);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getTrippingLog/{meterno}/{date}")]
        List<trippingLog> getTrippingLogData(string meterno, string date);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getVoltageGraph/{meterno}/{status}")]
        List<voltageGraph> getVoltageGrpahData(string meterno, string status);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getCurrentGraph/{meterno}/{status}")]
        List<currentGraph> getCurrentGraphData(string meterno, string status);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getPowerGraph/{meterno}")]
        List<powerGraph> getPowerGraphData(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getEnergyGraph/{meterno}/{status}")]
        List<energyGraph> getEnergyGrpahData(string meterno, string status);
        //DB_View_Feeder_LoadGraph
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getLoadGraph/{meterno}/{status}")]
        List<loadGraph> getLoadGraphData(string meterno, string status);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getMonthlySupply/{meterno}/{month}/{status}")]
        List<monthlyGraph> getMonthlyGraphData(string meterno, string month, string status);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getInstants/{meterno}")]
        List<instants> getMeterInstants(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/getSMSStatus/{meterno}")]
        List<sp_smsStatus> smsStatus(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/PhaseHistoryData/{eventstatus}/{meterno}/{fromdate}/{todate}")]
        List<sp_phasehistories> GetPhaseHistoryData(string eventstatus, string meterno, string fromdate, string todate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/HalfPeakloadHistoryData/{meterno}/{fromdate}/{todate}")]
        List<sp_HalfHourlyPeakload> GetHalfPeakloadHistoryData(string meterno, string fromdate, string todate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/SubstationPeakloadHistoryData/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}/{substation}")]
        List<sp_daywisesubstationpeakload> GetSubstationPeakloadHistoryData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string substation);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/PFAbstractHistoryData/{meterno}/{fromdate}/{todate}/{pf}")]
        List<sp_PfAbstractHistory> GetPFAbstractHistoryData(string meterno, string fromdate, string todate, string pf);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/OverloadHistoryData/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}")]
        List<sp_overUnderload> GetOverloadHistoryData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/OverloadParentData/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}/{substation}/{load}")]
        List<sp_overloadParent> GetOverloadParentData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string substation, string load);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/OverloadChildData/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}/{meterno}/{load}")]
        List<sp_overloadChild> GetOverloadChildData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string meterno, string load);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/InterruptionHistory/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}")]
        List<sp_feederInterruption> GetFeederInterruptionHistory(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/InterruptionParentData/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}/{substation}/{pfspell}")]
        List<sp_overloadParent> GetFeederInterruptionParentData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string substation, string pfspell);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/InterruptionChildData/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}/{meterno}/{pfspell}")]
        List<sp_interruptionChild> GetFeederInterruptionChildData(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string meterno, string pfspell);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/MeterInstantsHistory/{meterno}/{fromdate}/{todate}")]
        List<sp_feederInstants> GetMeterInstantsHistory(string meterno, string fromdate, string todate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/LogbookHistory/{userid}/{zone}/{circle}/{mla}/{division}/{subdivision}/{jen}/{fromdate}/{todate}/{meterno}")]
        List<sp_LogChild> GetLogBookHistory(string userid, string zone, string circle, string mla, string division, string subdivision, string jen, string fromdate, string todate, string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/PhaseAreawiseData/{substation}/{feedername}")]
        List<sp_areawise> GetAreawiseData(string substation, string feedername);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DrpSubstation/{user}")]
        List<sp_drpsubstations> GetDropdownSubstationData(string user);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/LiveFeedersData/{substation}/{pdate}")]
        List<sp_livefeedersdata> GetLiveFeedersData(string substation, string pdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/LiveFeederNamesData/{substation}")]
        List<sp_livefeedername> GetLiveFeederNamesData(string substation);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/LiveSummaryData/{substation}/{pdate}")]
        List<sp_livesummary> GetLiveSummaryData(string substation, string pdate);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/PowerFactorGraph/{meterno}")]
        List<sp_powerfactorgraph> GetPowerFactorGraph(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/HalfPeakloadGraph/{meterno}")]
        List<sp_peakloadgraph> GetHalfPeakloadGraph(string meterno);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/SubstationPeakloadGraph/{substation}")]
        List<sp_peakloadgraph> GetSubstationPeakloadGraph(string substation);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/OverloadGraph/{substation}")]
        List<sp_overloadgraph> GetOverloadGraph(string substation);

        // MIS Reports 

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseEnergyZone/{userid}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseAllData(string userid, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseZoneHistory/{userid}/{zone}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseZoneHistoryData(string userid, string zone, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseEnergyCircle/{userid}/{zone}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseCircleData(string userid, string zone, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseCircleHistory/{userid}/{zone}/{circle}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseCircleHistoryData(string userid, string zone, string circle, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseEnergyDivision/{userid}/{zone}/{circle}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseDivisionData(string userid, string zone, string circle, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseDivisionHistory/{userid}/{zone}/{circle}/{division}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseDivisionHistoryData(string userid, string zone, string circle, string division, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseEnergySubDivision/{userid}/{zone}/{circle}/{division}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseSubDivisionData(string userid, string zone, string circle, string division, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseSubDivisionHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseSubDivisionHistoryData(string userid, string zone, string circle, string division, string subdivision, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseEnergyJen/{userid}/{zone}/{circle}/{division}/{subdivision}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseJenHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseSectionHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseEnergySubstation/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/FeederwiseSubstationHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{substation}/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsZone/{userid}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingAllData(string userid, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsZoneHistory/{userid}/{zone}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingZonewiseHistoryData(string userid, string zone, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsCircle/{userid}/{zone}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingCircleData(string userid, string zone, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsCircleHistory/{userid}/{zone}/{circle}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingCirclewiseHistoryData(string userid, string zone, string circle, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsDivision/{userid}/{zone}/{circle}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingDivisionData(string userid, string zone, string circle, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsDivisionHistory/{userid}/{zone}/{circle}/{division}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingDivisionwiseHistoryData(string userid, string zone, string circle, string division, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsSubdivision/{userid}/{zone}/{circle}/{division}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingSubDivisionData(string userid, string zone, string circle, string division, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsSubdivisionHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingSubDivwiseHistoryData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsJen/{userid}/{zone}/{circle}/{division}/{subdivision}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsJenHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingSectionHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsSubstation/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/TrippingsSubstationHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{substation}/{monthyear}/{type}")]
        List<sp_tripppings> GetTrippingSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLDrawlZone/{userid}/{monthyear}/{type}")]
        List<sp_distributionlosses_drawl> GetDLDrawlandSaleAllData(string userid, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLDrawlCircle/{userid}/{zone}/{monthyear}/{type}")]
        List<sp_distributionlosses_drawl> GetDLDrawlandSaleCircleData(string userid, string zone, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLDrawlDivision/{userid}/{zone}/{circle}/{monthyear}/{type}")]
        List<sp_distributionlosses_drawl> GetDLDrawlandSaleDivisionData(string userid, string zone, string circle, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLDrawlSubdivision/{userid}/{zone}/{circle}/{division}/{monthyear}/{type}")]
        List<sp_distributionlosses_drawl> GetDLDrawlandSaleSubDivisionData(string userid, string zone, string circle, string division, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DlDrawlJen/{userid}/{zone}/{circle}/{division}/{subdivision}/{monthyear}/{type}")]
        List<sp_distributionlosses_drawl> GetDLDrawlandSaleSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLDrawlSubstation/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{monthyear}/{type}")]
        List<sp_distributionlosses_drawl> GetDLDrawlandSaleSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLDrawlSubstationHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{substation}/{monthyear}/{type}")]
        List<sp_distributionlosses_drawl> GetDLDrawlandSaleSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisZone/{userid}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionAllData(string userid, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisZoneHistory/{userid}/{zone}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionZonewiseHistoryData(string userid, string zone, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisCircle/{userid}/{zone}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionCircleData(string userid, string zone, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisCircleHistory/{userid}/{zone}/{circle}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionCirclewiseHistoryData(string userid, string zone, string circle, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisDivision/{userid}/{zone}/{circle}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionDivisionData(string userid, string zone, string circle, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisDivisionHistory/{userid}/{zone}/{circle}/{division}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionDivisionwiseHistoryData(string userid, string zone, string circle, string division, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisSubdivision/{userid}/{zone}/{circle}/{division}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionSubDivisionData(string userid, string zone, string circle, string division, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisSubdivisionHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionSubDivwiseHistoryData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisJen/{userid}/{zone}/{circle}/{division}/{subdivision}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionSectionData(string userid, string zone, string circle, string division, string subdivision, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisJenHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionSectionHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisSubstation/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionSubstationData(string userid, string zone, string circle, string division, string subdivision, string jen, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/DLAnalysisSubstationHistory/{userid}/{zone}/{circle}/{division}/{subdivision}/{jen}/{substation}/{monthyear}/{type}")]
        List<sp_distributionlosses_analysis> GetDistributionSubstationHistoryData(string userid, string zone, string circle, string division, string subdivision, string jen, string substation, string monthyear, string type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/QualityPower/{monthyear}")]
        List<sp_qualitypower> GetQualityPowerSupplyData(string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/LRPMeasure/{monthyear}")]
        List<sp_lrpmeasures> GetLRPMeasuresActionData(string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/SLA/{monthyear}")]
        List<sp_sla> GetSLAData(string monthyear);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/SLAHistory/{monthyear}/{parm}/{zone}/{circle}")]
        List<sp_sla> GetSLAHistoryData(string monthyear,string parm,string zone,string circle);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/Installation")]
        List<sp_install> GetFeederInstallationData();

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/AuditSummary/{monthyear}")]
        List<sp_feederwiseenergy> GetFeederwiseAuditSummaryData(string monthyear);

        //Ending MIS Reports 
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.

    [DataContract]
    public class feederDetails
    {
        [DataMember]
        public string meterno { get; set; }
        [DataMember]
        public string feederNo { get; set; }
        [DataMember]
        public string feederName { get; set; }
        [DataMember]
        public string zone { get; set; }
        [DataMember]
        public string circle { get; set; }
        [DataMember]
        public string mla { get; set; }
        [DataMember]
        public string division { get; set; }
        [DataMember]
        public string subdivision { get; set; }
        [DataMember]
        public string jen { get; set; }
        [DataMember]
        public string substation { get; set; }
        [DataMember]
        public string substation_ph { get; set; }
        [DataMember]
        public string feederType { get; set; }
        [DataMember]
        public string village { get; set; }
        [DataMember]
        public string latitude { get; set; }
        [DataMember]
        public string longitude { get; set; }
        [DataMember]
        public string ce_name { get; set; }
        [DataMember]
        public string ce_ph { get; set; }
        [DataMember]
        public string se_name { get; set; }
        [DataMember]
        public string se_ph { get; set; }
        [DataMember]
        public string xen_name { get; set; }
        [DataMember]
        public string xen_ph { get; set; }
        [DataMember]
        public string aen_name { get; set; }
        [DataMember]
        public string aen_ph { get; set; }
        [DataMember]
        public string jen_name { get; set; }
        [DataMember]
        public string jen_ph { get; set; }
        [DataMember]
        public string fi_name { get; set; }
        [DataMember]
        public string fi_ph { get; set; }
        [DataMember]
        public string mla_name { get; set; }
        [DataMember]
        public string mla_ph { get; set; }
        [DataMember]
        public string modem_sno { get; set; }
        [DataMember]
        public string modem_ph { get; set; }
        [DataMember]
        public string metertype { get; set; }
        [DataMember]
        public string meterFirm { get; set; }
        [DataMember]
        public string YOM { get; set; }
        [DataMember]
        public string ctr_inter { get; set; }
        [DataMember]
        public string ptr_inter { get; set; }
        [DataMember]
        public string MF { get; set; }
        [DataMember]
        public string area { get; set; }
    }

    [DataContract]
    public class feederSummary
    {
        [DataMember]
        public string maxMDKw { get; set; }
        [DataMember]
        public string MinMDKw { get; set; }
        [DataMember]
        public string AvgMdKW { get; set; }
        [DataMember]
        public string kWh { get; set; }
        [DataMember]
        public string kVAh { get; set; }
        [DataMember]
        public string kVArh_lag { get; set; }
        [DataMember]
        public string kVArh_lead { get; set; }
        [DataMember]
        public string maxPF { get; set; }
        [DataMember]
        public string minPF { get; set; }
        [DataMember]
        public string avgPF { get; set; }
        [DataMember]
        public string minMDkVA { get; set; }
        [DataMember]
        public string AvgMDkVA { get; set; }
        [DataMember]
        public string MaxMDkVA { get; set; }
        [DataMember]
        public string MaxVolt { get; set; }
        [DataMember]
        public string MinVolt { get; set; }
        [DataMember]
        public string AvgVolt { get; set; }
        [DataMember]
        public string MaxCur { get; set; }
        [DataMember]
        public string MinCur { get; set; }
        [DataMember]
        public string AvgCur { get; set; }
        [DataMember]
        public string TotalInt { get; set; }
        [DataMember]
        public string TotalIntDur { get; set; }
        [DataMember]
        public string HighDur { get; set; }
        [DataMember]
        public string lowDur { get; set; }
        [DataMember]
        public string AG_Cons { get; set; }
        [DataMember]
        public string DL_Cons { get; set; }
        [DataMember]
        public string Three_ph { get; set; }
        [DataMember]
        public string Single_ph { get; set; }
    }

    [DataContract]
    public class meterEvents
    {
        [DataMember]
        public string Sno { get; set; }
        [DataMember]
        public string meterno { get; set; }
        [DataMember]
        public string edate { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string vr { get; set; }
        [DataMember]
        public string vy { get; set; }
        [DataMember]
        public string vb { get; set; }
        [DataMember]
        public string vry { get; set; }
        [DataMember]
        public string vyb { get; set; }
        [DataMember]
        public string ir { get; set; }
        [DataMember]
        public string iy { get; set; }
        [DataMember]
        public string ib { get; set; }
        [DataMember]
        public string qr { get; set; }
        [DataMember]
        public string qy { get; set; }
        [DataMember]
        public string qb { get; set; }
        [DataMember]
        public string cum_kWh { get; set; }

    }

    [DataContract]
    public class loadSurvey
    {
        [DataMember]
        public string Sno { get; set; }
        [DataMember]
        public string Meterno { get; set; }
        [DataMember]
        public string mdate { get; set; }
        [DataMember]
        public string vr { get; set; }
        [DataMember]
        public string vy { get; set; }
        [DataMember]
        public string vb { get; set; }
        [DataMember]
        public string ir { get; set; }
        [DataMember]
        public string iy { get; set; }
        [DataMember]
        public string ib { get; set; }
        [DataMember]
        public string kwh { get; set; }
        [DataMember]
        public string kvarh_lg { get; set; }
        [DataMember]
        public string kvarh_ld { get; set; }
        [DataMember]
        public string kvarh { get; set; }
        [DataMember]
        public string freq { get; set; }
        [DataMember]
        public string meterDate { get; set; }

    }

    [DataContract]
    public class dailyLoad
    {
        [DataMember]
        public string Sno { get; set; }
        [DataMember]
        public string Meterno { get; set; }
        [DataMember]
        public string mdate { get; set; }
        [DataMember]
        public string cumkwh_imp { get; set; }
        [DataMember]
        public string cumkwh_exp { get; set; }
        [DataMember]
        public string cumkvah_kwimp { get; set; }
        [DataMember]
        public string cumkvah_kwexp { get; set; }
        [DataMember]
        public string cumkvarh_q1 { get; set; }
        [DataMember]
        public string cumkvarh_q2 { get; set; }
        [DataMember]
        public string cumkvarh_q3 { get; set; }
        [DataMember]
        public string cumkvarh_q4 { get; set; }
        [DataMember]
        public string meterDate { get; set; }
    }

    [DataContract]
    public class supplyLog
    {
        [DataMember(Order = 0)]
        public string Sno { get; set; }
        [DataMember(Order = 1)]
        public string date { get; set; }
        [DataMember(Order = 2)]
        public string threePhaseDuration { get; set; }
        [DataMember(Order = 3)]
        public string threePhaseTimeDuration { get; set; }
        [DataMember(Order = 4)]
        public string twoPhaseDuration { get; set; }
        [DataMember(Order = 5)]
        public string twoPhaseTimeDuraton { get; set; }
        [DataMember(Order = 6)]
        public string singlePhaseDuration { get; set; }
        [DataMember(Order = 7)]
        public string singlePhaseTimeDuration { get; set; }
        [DataMember(Order = 8)]
        public string ourtageDuration { get; set; }
        [DataMember(Order = 9)]
        public string outageTimeDuration { get; set; }
        [DataMember(Order = 10)]
        public string totalSupplyDuration { get; set; }
        [DataMember(Order = 11)]
        public string remarks { get; set; }
    }

    [DataContract]
    public class mergegrid
    {
        [DataMember]
        public string tr_ph_con { get; set; }
        [DataMember]
        public string tw_ph_con { get; set; }
        [DataMember]
        public string si_ph_con { get; set; }
    }

    [DataContract]
    public class trippingLog
    {
        [DataMember(Order = 0)]
        public string date { get; set; }
        [DataMember(Order = 1)]
        public string th_ph_tripping { get; set; }
        [DataMember(Order = 2)]
        public string th_ph_tripping_l10 { get; set; }
        [DataMember(Order = 3)]
        public string tw_ph_tripping { get; set; }
        [DataMember(Order = 4)]
        public string tw_ph_tripping_l10 { get; set; }
        [DataMember(Order = 5)]
        public string si_ph_tripping { get; set; }
        [DataMember(Order = 6)]
        public string si_ph_tripping_l10 { get; set; }
        [DataMember(Order = 7)]
        public string tot_ph_tripping { get; set; }
        [DataMember(Order = 8)]
        public string tot_ph_tripping_l10 { get; set; }
        [DataMember(Order = 9)]
        public string remarks { get; set; }
    }

    [DataContract]
    public class voltageGraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 2)]
        public string vr { get; set; }
        [DataMember(Order = 3)]
        public string vy { get; set; }
        [DataMember(Order = 4)]
        public string vb { get; set; }
    }

    [DataContract]
    public class currentGraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 1)]
        public string Ir { get; set; }
        [DataMember(Order = 2)]
        public string Iy { get; set; }
        [DataMember(Order = 3)]
        public string Ib { get; set; }
    }

    [DataContract]
    public class monthlyGraph
    {
        [DataMember(Order = 0)]
        public string zone { get; set; }
        [DataMember(Order = 1)]
        public string circle { get; set; }
        [DataMember(Order = 2)]
        public string division { get; set; }
        [DataMember(Order = 3)]
        public string subDivision { get; set; }
        [DataMember(Order = 4)]
        public string section { get; set; }
        [DataMember(Order = 5)]
        public string subStation { get; set; }
        [DataMember(Order = 6)]
        public string feederName { get; set; }
        [DataMember(Order = 7)]
        public string feederNo { get; set; }
        [DataMember(Order = 8)]
        public string meterNo { get; set; }
        [DataMember(Order = 9)]
        public string th_Ph { get; set; }
        [DataMember(Order = 10)]
        public string tw_ph { get; set; }
        [DataMember(Order = 11)]
        public string si_ph { get; set; }
        [DataMember(Order = 12)]
        public string powerFail { get; set; }
        [DataMember(Order = 13)]
        public string notCom { get; set; }
        [DataMember(Order = 14)]
        public string th_ph_Dur { get; set; }
        [DataMember(Order = 15)]
        public string tw_ph_Dur { get; set; }
        [DataMember(Order = 16)]
        public string si_ph_Dur { get; set; }
        [DataMember(Order = 17)]
        public string powerFail_Dur { get; set; }
        [DataMember(Order = 18)]
        public string not_comm_Dur { get; set; }

    }

    [DataContract]
    public class loadGraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 1)]
        public string kw { get; set; }
        [DataMember(Order = 2)]
        public string kva { get; set; }
    }

    [DataContract]
    public class energyGraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 1)]
        public string kwh { get; set; }
        [DataMember(Order = 2)]
        public string kvah { get; set; }
        [DataMember(Order = 3)]
        public string kvarh_lag { get; set; }
        [DataMember(Order = 4)]
        public string kvarh_lead { get; set; }
    }

    [DataContract]
    public class powerGraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 1)]
        public string pf { get; set; }

    }

    [DataContract]
    public class sp_smsStatus
    {
        [DataMember(Order = 0)]
        public string supplyStatus { get; set; }
        [DataMember(Order = 1)]
        public string SMS_status { get; set; }
    }

    [DataContract]
    public class instants
    {
        [DataMember(Order = 0)]
        public string vr { get; set; }
        [DataMember(Order = 1)]
        public string vy { get; set; }
        [DataMember(Order = 2)]
        public string vb { get; set; }
        [DataMember(Order = 3)]
        public string ir { get; set; }
        [DataMember(Order = 4)]
        public string iy { get; set; }
        [DataMember(Order = 5)]
        public string ib { get; set; }
        [DataMember(Order = 6)]
        public string qr { get; set; }
        [DataMember(Order = 7)]
        public string qy { get; set; }
        [DataMember(Order = 8)]
        public string qb { get; set; }
        [DataMember(Order = 9)]
        public string meter_date { get; set; }
        [DataMember(Order = 10)]
        public string kw { get; set; }
        [DataMember(Order = 11)]
        public string kvar { get; set; }
        [DataMember(Order = 12)]
        public string kva { get; set; }
        [DataMember(Order = 13)]
        public string qavg { get; set; }
        [DataMember(Order = 14)]
        public string freq { get; set; }
        [DataMember(Order = 15)]
        public string cum_kwh { get; set; }
        [DataMember(Order = 16)]
        public string cum_kvarh_lg { get; set; }
        [DataMember(Order = 17)]
        public string cum_kvarh_ld { get; set; }
        [DataMember(Order = 18)]
        public string cum_kvah { get; set; }
        [DataMember(Order = 19)]
        public string mdKW { get; set; }
        [DataMember(Order = 20)]
        public string mdkw_date { get; set; }
        [DataMember(Order = 21)]
        public string mdkva { get; set; }
        [DataMember(Order = 22)]
        public string mdkva_date { get; set; }
        [DataMember(Order = 23)]
        public string powerFailCount { get; set; }
        [DataMember(Order = 24)]
        public string cum_pwr_fail_dur { get; set; }
        [DataMember(Order = 25)]
        public string bill_date { get; set; }
        [DataMember(Order = 26)]
        public string cum_tmpr_count { get; set; }
        [DataMember(Order = 27)]
        public string cum_prog_count { get; set; }
        [DataMember(Order = 18)]
        public string cum_bill_count { get; set; }
    }

    [DataContract]
    public class userMaster
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class sp_LogChild
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string Area { get; set; }
        [DataMember(Order = 5)]
        public string Meterno { get; set; }
        [DataMember(Order = 6)]
        public string MF { get; set; }
        [DataMember(Order = 7)]
        public string DateTime { get; set; }
        [DataMember(Order = 8)]
        public string load_cur { get; set; }
        [DataMember(Order = 9)]
        public string load_kva { get; set; }
        [DataMember(Order = 10)]
        public string Reading { get; set; }
        [DataMember(Order = 11)]
        public string kwh_Cons { get; set; }
        [DataMember(Order = 12)]
        public string Ir { get; set; }
        [DataMember(Order = 13)]
        public string Iy { get; set; }
        [DataMember(Order = 14)]
        public string Ib { get; set; }
        [DataMember(Order = 15)]
        public string Vr { get; set; }
        [DataMember(Order = 16)]
        public string Vy { get; set; }
        [DataMember(Order = 17)]
        public string Vb { get; set; }
        [DataMember(Order = 18)]
        public string kvarh_lag { get; set; }
        [DataMember(Order = 19)]
        public string kvarh_lead { get; set; }
        [DataMember(Order = 20)]
        public string PF { get; set; }
        [DataMember(Order = 21)]
        public string EventDesc { get; set; }

    }

    [DataContract]
    public class sp_LogParent
    {
        [DataMember]
        public string Substation { get; set; }
        [DataMember]
        public string FeederName { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Area { get; set; }
        [DataMember]
        public string Meterno { get; set; }
    }

    [DataContract]
    public class sp_feederNotCommunicating
    {
        [DataMember]
        public string SNo { get; set; }
        [DataMember]
        public string Zone { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string MLA { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string SubDivision { get; set; }
        [DataMember]
        public string Section { get; set; }
        [DataMember]
        public string SubStation { get; set; }
        [DataMember]
        public string FeederName { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Area { get; set; }
        [DataMember]
        public string Meterno { get; set; }
        [DataMember]
        public string MF { get; set; }
        [DataMember]
        public string RegDate { get; set; }
        [DataMember]
        public string Schdule { get; set; }
        [DataMember]
        public string CommDate { get; set; }
        [DataMember]
        public string EventStatus { get; set; }
        [DataMember]
        public string Remarks { get; set; }
    }

    [DataContract]
    public class sp_feederInstants
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string Area { get; set; }
        [DataMember(Order = 5)]
        public string MeterNo { get; set; }
        [DataMember(Order = 6)]
        public string MF { get; set; }
        [DataMember(Order = 7)]
        public string RTC { get; set; }
        [DataMember(Order = 8)]
        public string ActEng_CumFWD { get; set; }
        [DataMember(Order = 9)]
        public string ReaEng_CumFWD { get; set; }
        [DataMember(Order = 10)]
        public string ReaEng_CumREV { get; set; }
        [DataMember(Order = 11)]
        public string AppEng_CumFWD { get; set; }
        [DataMember(Order = 12)]
        public string avgPF_FWD { get; set; }
        [DataMember(Order = 13)]
        public string Freq { get; set; }
    }

    [DataContract]
    public class sp_feederInterruption
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string MeterDate { get; set; }
        [DataMember(Order = 3)]
        public string PF1 { get; set; }
        [DataMember(Order = 4)]
        public string PF2 { get; set; }
        [DataMember(Order = 5)]
        public string PF3 { get; set; }
        [DataMember(Order = 6)]
        public string PF4 { get; set; }
    }

    [DataContract]
    public class sp_interruptionChild
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string METERNO { get; set; }
        [DataMember(Order = 5)]
        public string Startdate { get; set; }
        [DataMember(Order = 6)]
        public string EndDate { get; set; }
        [DataMember(Order = 7)]
        public string Duration { get; set; }
    }
    [DataContract]
    public class sp_overUnderload
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string MeterDate { get; set; }
        [DataMember(Order = 3)]
        public string Overload { get; set; }
        [DataMember(Order = 4)]
        public string Underload { get; set; }
    }
    [DataContract]
    public class sp_overloadParent
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string METERNO { get; set; }
        [DataMember(Order = 5)]
        public string spells { get; set; }
    }
    [DataContract]
    public class sp_overloadChild
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string METERNO { get; set; }
        [DataMember(Order = 5)]
        public string Meterdate { get; set; }
        [DataMember(Order = 6)]
        public string Ir { get; set; }
        [DataMember(Order = 7)]
        public string Iy { get; set; }
        [DataMember(Order = 8)]
        public string Ib { get; set; }
        [DataMember(Order = 9)]
        public string AvgCur { get; set; }
    }
    [DataContract]
    public class sp_PfAbstract
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string Area { get; set; }
        [DataMember(Order = 5)]
        public string Meterno { get; set; }
        [DataMember(Order = 6)]
        public string AbovePF { get; set; }
        [DataMember(Order = 7)]
        public string BelowPf { get; set; }
    }

    [DataContract]
    public class sp_HalfHourlyPeakload
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SUbStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string Area { get; set; }
        [DataMember(Order = 5)]
        public string MeterNo { get; set; }
        [DataMember(Order = 6)]
        public string MF { get; set; }
        [DataMember(Order = 7)]
        public string RTC { get; set; }
        [DataMember(Order = 8)]
        public string Ir { get; set; }
        [DataMember(Order = 9)]
        public string Iy { get; set; }
        [DataMember(Order = 10)]
        public string Ib { get; set; }
        [DataMember(Order = 11)]
        public string AvgCur { get; set; }
        [DataMember(Order = 12)]
        public string kVA { get; set; }
    }

    [DataContract]
    public class sp_daywisesubstationpeakload
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string substation { get; set; }
        [DataMember(Order = 2)]
        public string meterdate { get; set; }
        [DataMember(Order = 3)]
        public string Ir { get; set; }
        [DataMember(Order = 4)]
        public string Iy { get; set; }
        [DataMember(Order = 5)]
        public string Ib { get; set; }
        [DataMember(Order = 6)]
        public string kVa { get; set; }
        [DataMember(Order = 7)]
        public string AvgCur { get; set; }
    }

    [DataContract]
    public class sp_Feeder_loadSurvey
    {
        [DataMember]
        public string sno { get; set; }
        [DataMember]
        public string meterno { get; set; }
        [DataMember]
        public string meterDate { get; set; }
        [DataMember]
        public string vr { get; set; }
        [DataMember]
        public string vy { get; set; }
        [DataMember]
        public string vb { get; set; }
        [DataMember]
        public string ir { get; set; }
        [DataMember]
        public string iy { get; set; }
        [DataMember]
        public string ib { get; set; }
        [DataMember]
        public string kvarh_lag { get; set; }
        [DataMember]
        public string kvarh_lead { get; set; }
        [DataMember]
        public string kvah { get; set; }
        [DataMember]
        public string frequ { get; set; }


    }

    [DataContract]
    public class sp_zone_result
    {
        [DataMember]
        public string zone { get; set; }
    }

    [DataContract]
    public class sp_circle_result
    {
        [DataMember]
        public string circle { get; set; }
    }

    [DataContract]
    public class sp_mla_result
    {
        [DataMember]
        public string mla { get; set; }
    }

    [DataContract]
    public class sp_division_result
    {
        [DataMember]
        public string div { get; set; }
    }

    [DataContract]
    public class sp_subdiv_result
    {
        [DataMember]
        public string subdiv { get; set; }
    }

    [DataContract]
    public class sp_jen_result
    {
        [DataMember]
        public string jen { get; set; }
    }

    [DataContract]
    public class sp_piedata
    {
        [DataMember(Order = 0)]
        public string singlephase { get; set; }
        [DataMember(Order = 1)]
        public string twophase { get; set; }
        [DataMember(Order = 2)]
        public string threephase { get; set; }
        [DataMember(Order = 3)]
        public string nophase { get; set; }
        [DataMember(Order = 4)]
        public string notcomm { get; set; }
        [DataMember(Order = 5)]
        public string total { get; set; }
    }

    [DataContract]
    public class sp_substations
    {
        [DataMember(Name = "substation", Order = 0)]
        public string substation { get; set; }
        [DataMember(Name = "feeders", Order = 1)]
        public List<sp_feeders> feeders { get; set; }
        [DataMember(Name = "colour", Order = 2)]
        public string colour { get; set; }
    }
    [DataContract]
    public class sp_feeders
    {
        [DataMember(Name = "feeder")]
        public string feeder { get; set; }
    }
    [DataContract]
    public class sp_phases
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string Substation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string Area { get; set; }
        [DataMember(Order = 5)]
        public string MeterNo { get; set; }
        [DataMember(Order = 6)]
        public string MF { get; set; }
        [DataMember(Order = 7)]
        public string RTC { get; set; }
        [DataMember(Order = 8)]
        public string Duration { get; set; }
        [DataMember(Order = 9)]
        public string Spells { get; set; }
    }
    [DataContract]
    public class sp_phasehistories
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string Substation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string Area { get; set; }
        [DataMember(Order = 5)]
        public string MeterNo { get; set; }
        [DataMember(Order = 6)]
        public string MF { get; set; }
        [DataMember(Order = 7)]
        public string StartDate { get; set; }
        [DataMember(Order = 8)]
        public string EndDate { get; set; }
        [DataMember(Order = 9)]
        public string Duration { get; set; }
    }
    [DataContract]
    public class sp_areawise
    {
        [DataMember(Order = 0)]
        public string zone { get; set; }
        [DataMember(Order = 1)]
        public string circle { get; set; }
        [DataMember(Order = 2)]
        public string mla { get; set; }
        [DataMember(Order = 3)]
        public string division { get; set; }
        [DataMember(Order = 4)]
        public string subdivision { get; set; }
        [DataMember(Order = 5)]
        public string section { get; set; }
    }
    [DataContract]
    public class sp_PfAbstractHistory
    {
        [DataMember(Order = 0)]
        public string SNo { get; set; }
        [DataMember(Order = 1)]
        public string SubStation { get; set; }
        [DataMember(Order = 2)]
        public string FeederName { get; set; }
        [DataMember(Order = 3)]
        public string Type { get; set; }
        [DataMember(Order = 4)]
        public string Area { get; set; }
        [DataMember(Order = 5)]
        public string Meterno { get; set; }
        [DataMember(Order = 6)]
        public string meterdate { get; set; }
        [DataMember(Order = 7)]
        public string PF { get; set; }
    }
    [DataContract]
    public class sp_drpsubstations
    {
        [DataMember]
        public string substation { get; set; }
    }
    [DataContract]
    public class sp_livefeedersdata
    {
        [DataMember(Order = 0, Name = "name")]
        public string EVENTSTATUS { get; set; }
        [DataMember(Order = 1, Name = "x")]
        public int reserved { get; set; }
        [DataMember(Order = 2, Name = "low")]
        public double STARTDATE { get; set; }
        [DataMember(Order = 3, Name = "high")]
        public double ENDDATE { get; set; }
        [DataMember(Order = 4, Name = "color")]
        public string color { get; set; }
    }
    [DataContract]
    public class sp_livefeedername
    {
        [DataMember]
        public string FeederName { get; set; }
    }
    [DataContract]
    public class sp_livesummary
    {
        [DataMember(Order = 0)]
        public string FeederName { get; set; }
        [DataMember(Order = 1)]
        public string FeederNo { get; set; }
        [DataMember(Order = 2)]
        public string MeterNo { get; set; }
        [DataMember(Order = 3)]
        public string FeederType { get; set; }
        [DataMember(Order = 4)]
        public string Dur_3Ph { get; set; }
        [DataMember(Order = 5)]
        public string Dur_2Ph { get; set; }
        [DataMember(Order = 6)]
        public string Dur_1Ph { get; set; }
        [DataMember(Order = 7)]
        public string Dur_PF { get; set; }
        [DataMember(Order = 8)]
        public string Dur_ND { get; set; }
    }
    [DataContract]
    public class sp_powerfactorgraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 1)]
        public string pf { get; set; }

    }
    [DataContract]
    public class sp_peakloadgraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 1)]
        public string kva { get; set; }

    }
    [DataContract]
    public class sp_overloadgraph
    {
        [DataMember(Order = 0)]
        public string time { get; set; }
        [DataMember(Order = 1)]
        public string AvgCurrent { get; set; }
    }
    [DataContract]
    public class sp_feederwiseenergy
    {
        [DataMember]
        public int SNo { get; set; }
        [DataMember]
        public string Zone { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string SubDivision { get; set; }
        [DataMember]
        public string Section { get; set; }
        [DataMember]
        public string Substation { get; set; }
        [DataMember]
        public string FeederName { get; set; }
        [DataMember]
        public string FeederNo { get; set; }
        [DataMember]
        public string MeterNo { get; set; }
        [DataMember]
        public int FeederNos { get; set; }
        [DataMember]
        public float? TotalUnits { get; set; }
        [DataMember]
        public float? T1 { get; set; }
        [DataMember]
        public float? T2 { get; set; }
        [DataMember]
        public string BA { get; set; }
        [DataMember]
        public string CA { get; set; }
        [DataMember]
        public string ATCloss { get; set; }
    }
    [DataContract]
    public class sp_tripppings
    {
        [DataMember]
        public int SNo { get; set; }
        [DataMember]
        public string Zone { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string SubDivision { get; set; }
        [DataMember]
        public string Section { get; set; }
        [DataMember]
        public string Substation { get; set; }
        [DataMember]
        public string FeederName { get; set; }
        [DataMember]
        public string FeederNo { get; set; }
        [DataMember]
        public string MeterNo { get; set; }
        [DataMember]
        public int FeederNos { get; set; }
        [DataMember]
        public int TotalTrippings { get; set; }
        [DataMember]
        public int Lessthan3Trippings { get; set; }
        [DataMember]
        public int AverageTrippings { get; set; }
    }
    [DataContract]
    public class sp_distributionlosses_drawl
    {
        [DataMember]
        public int SNo { get; set; }
        [DataMember]
        public string Zone { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string SubDivision { get; set; }
        [DataMember]
        public string Section { get; set; }
        [DataMember]
        public string SubStation { get; set; }
        [DataMember]
        public string FeederName { get; set; }
        [DataMember]
        public string FeederNo { get; set; }
        [DataMember]
        public string MeterNo { get; set; }
        [DataMember]
        public int FeederNos { get; set; }
        [DataMember]
        public float? TED_FortheMonth { get; set; }
        [DataMember]
        public float? TED_UptotheMonth { get; set; }
        [DataMember]
        public float? TES_FortheMonth { get; set; }
        [DataMember]
        public float? TES_UptotheMonth { get; set; }
        [DataMember]
        public float? AED_DuringtheMonth_kWh { get; set; }
        [DataMember]
        public float? AED_UptotheMonth_kWh { get; set; }
        [DataMember]
        public float? AES_DuringtheMonth_BilledEnergy { get; set; }
        [DataMember]
        public float? AES_UptotheMonth_BilledEnergy { get; set; }
        [DataMember]
        public float? EnergyDrawlVariation { get; set; }
        [DataMember]
        public float? EnergySoldVariation { get; set; }

    }
    [DataContract]
    public class sp_distributionlosses_analysis
    {
        [DataMember]
        public int SNo { get; set; }
        [DataMember]
        public string Zone { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string SubDivision { get; set; }
        [DataMember]
        public string Section { get; set; }
        [DataMember]
        public string Substation { get; set; }
        [DataMember]
        public string FeederName { get; set; }
        [DataMember]
        public string FeederNo { get; set; }
        [DataMember]
        public string MeterNo { get; set; }
        [DataMember]
        public int FeederNos { get; set; }
        [DataMember]
        public float? DTM_ED { get; set; }
        [DataMember]
        public float? DTM_ES { get; set; }
        [DataMember]
        public float? UTM_ED { get; set; }
        [DataMember]
        public float? UTM_ES { get; set; }
        [DataMember]
        public float? DTM_TDLoss { get; set; }
        [DataMember]
        public float? UTM_TDLoss { get; set; }
        [DataMember]
        public float? UTM_MPWing { get; set; }
        [DataMember]
        public float? AsPerMPWing { get; set; }
        [DataMember]
        public float? AsPerOMWing { get; set; }
    }
    [DataContract]
    public class sp_qualitypower
    {
        [DataMember]
        public int Sno { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string BlockFeeders { get; set; }
        [DataMember]
        public string NonBlockFeeders { get; set; }
        [DataMember]
        public string IndustrialFeeders { get; set; }
        [DataMember]
        public string WWFeeders { get; set; }
        [DataMember]
        public string TotalNos { get; set; }
        [DataMember]
        public string DLSupplyNos { get; set; }
        [DataMember]
        public string Per_DLSupply { get; set; }
        [DataMember]
        public string AGSupplyNos { get; set; }
        [DataMember]
        public string Per_AGSupply { get; set; }
    }
    [DataContract]
    public class sp_lrpmeasures
    {
        [DataMember]
        public int Sno { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string TotalBlockFeeders { get; set; }
        [DataMember]
        public string PrevMonthViolations { get; set; }
        [DataMember]
        public string RepMonthViolations { get; set; }
        [DataMember]
        public string PrevMonthRPTViolations { get; set; }
        [DataMember]
        public string RepMonthRPTViolations { get; set; }
        [DataMember]
        public string Remarks { get; set; }
    }
    [DataContract]
    public class sp_sla
    {
        [DataMember]
        public int SNo { get; set; }
        [DataMember]
        public string Zone { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string Division { get; set; }
        [DataMember]
        public string SubDivision { get; set; }
        [DataMember]
        public string SubStation { get; set; }
        [DataMember]
        public string MeterNo { get; set; }
        [DataMember]
        public string FeederNo { get; set; }
        [DataMember]
        public string FeederName { get; set; }
        [DataMember]
        public string AMRPh { get; set; }
        [DataMember]
        public string ModemSno { get; set; }
        [DataMember]
        public string Commissioned { get; set; }
        [DataMember]
        public string DataReceived { get; set; }
        [DataMember]
        public string DataNotReceived { get; set; }
        [DataMember]
        public string SLA { get; set; }
        [DataMember]
        public string Remarks { get; set; }
    }
    [DataContract]
    public class sp_install
    {
        [DataMember]
        public int SNo { get; set; }
        [DataMember]
        public string Zone { get; set; }
        [DataMember]
        public string Circle { get; set; }
        [DataMember]
        public string RAPDRP { get; set; }
        [DataMember]
        public string NONRAPDRP { get; set; }
        [DataMember]
        public string Total { get; set; }
        [DataMember]
        public string InstallationsDone { get; set; }
        [DataMember]
        public string Registered { get; set; }
        [DataMember]
        public string NotRegistered { get; set; }
        [DataMember]
        public string DataReceived { get; set; }
        [DataMember]
        public string DataNotReceived { get; set; }
        [DataMember]
        public string Balance { get; set; }
        [DataMember]
        public string PendingPercentage { get; set; }
    }
}